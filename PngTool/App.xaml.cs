﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.IO;

namespace PngTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        // 添加启动参数的处理
        protected override void OnStartup(StartupEventArgs e)
        {
            MainWindow = new MainWindow(e.Args.FirstOrDefault());
            MainWindow.Show();
        }
    }
}
