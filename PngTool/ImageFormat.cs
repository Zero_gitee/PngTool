﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PngTool
{
    internal class SourceImage
    {
        public string FileName { get; set; }
        public Bitmap Source { get; set; }
        public int Zoom { get; set; }
        public int Angle { get; set; }
        public bool ReversalUpDown { get; set; }
        public bool ReversalLeftRight { get; set; }

        public SourceImage(string fileName)
        {
            FileName = fileName;
            using (Bitmap souFile = new Bitmap(fileName))
            {
                Source = new Bitmap(souFile.Width, souFile.Height);
                using (Graphics g = Graphics.FromImage(Source))
                {
                    g.DrawImage(souFile, 0, 0, souFile.Width, souFile.Height);
                }
            }
            Zoom = 100;
            Angle = 0;
            ReversalUpDown = false;
            ReversalLeftRight = false;
        }

        public string ToHintString()
        {
            string Ret = FileName;

            if (Zoom != 100) Ret += string.Format(@"
缩放{0}%", Zoom);
            if (Angle > 0) Ret += string.Format(@"
顺时针旋转{0}度", Math.Abs(Angle));
            if (Angle < 0) Ret += string.Format(@"
逆时针旋转{0}度", Math.Abs(Angle));
            if (ReversalUpDown) Ret += string.Format(@"
上下翻转");
            if (ReversalLeftRight) Ret += string.Format(@"
左右翻转");

            return Ret;
        }

        public Bitmap ToDisplayImage()
        {
            //计算尺寸
            int w = Source.Width;
            int h = Source.Height;
            //缩放
            w = w * Zoom / 100;
            h = h * Zoom / 100;
            //旋转
            while (Angle > 360) Angle -= 360;
            while (Angle < -360) Angle += 360;

            int wNew = w;
            int hNew = h;
            if ((Math.Abs(Angle) == 90) || (Math.Abs(Angle) == 270))
            {
                wNew = h;
                hNew = w;
            }

            Bitmap Ret = new Bitmap(wNew, hNew);
            using (Graphics g = Graphics.FromImage(Ret))
            {
                g.TranslateTransform(wNew / 2, hNew / 2);
                g.RotateTransform(Angle);
                if (ReversalUpDown) g.ScaleTransform(1, -1);
                if (ReversalLeftRight) g.ScaleTransform(-1, 1);
                g.DrawImage(Source, -w / 2, -h / 2, w, h);
            }
            return Ret;
        }
    }
}
