﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Xml.Serialization;

namespace ZmrSoft
{
    //数据和数据列表的基类，提供序列化、释放资源和深层复制的方法
    namespace Data
    {
        public interface IZmrData : IDisposable, ICloneable
        {

        }

        [Serializable()]
        public class Data : IZmrData
        {
            /// <summary>
            /// 标记Dispose是否被调用
            /// </summary>
            protected bool Disposed = false;

            /// <summary>
            /// 析构函数
            /// </summary>
            ~Data()
            {
                Dispose(false);
            }

            /// <summary>
            /// 深层复制(利用缓存区对象)
            /// </summary>
            static public Data CopyFrom(Data sou)
            {
                Data Ret = new Data();

                using (Stream S = new MemoryStream())
                {
                    BinaryFormatter B = new BinaryFormatter();
                    B.Serialize(S, sou);
                    S.Position = 0;
                    Ret = (Data)B.Deserialize(S);
                    S.Close();
                }

                return Ret;
            }

            /// <summary>
            ///保存到流
            /// </summary>
            static public void SaveToStream(Data sou, Stream SaveStream)
            {
                BinaryFormatter SaveFile = new BinaryFormatter();                            
                SaveFile.Serialize(SaveStream, sou);                                 
            }

            /// <summary>
            ///保存到文件
            /// </summary>
            static public void SaveToFile(Data sou, string FileName)
            {
                FileStream SaveFileStream = new FileStream(FileName, FileMode.Create);        
                try
                {
                    SaveToStream(sou, SaveFileStream);                                  
                }
                finally
                {
                    SaveFileStream.Close();                                                  
                }
            }

            /// <summary>
            ///保存到byte
            /// </summary>
            static public void SaveToByte(Data sou, byte[] SaveByte)
            {
                Stream SaveStream = new MemoryStream();      
                try
                {
                    SaveToStream(sou, SaveStream);                                  
                    SaveStream.Position = 0;
                    SaveStream.Read(SaveByte, 0, (int)SaveStream.Length);
                }
                finally
                {
                    SaveStream.Close();                                                   
                }
            }

            /// <summary>
            ///保存到Xml
            /// </summary>
            static public void SaveToXml(Data sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(sou.GetType());
                FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    ser.Serialize(stream, sou);
                }
                finally
                {
                    stream.Close();
                }
            }

            /// <summary>
            ///从流读取
            /// </summary>
            static public void LoadFromStream(ref Data des, Stream ReadStream)
            {
                BinaryFormatter OpenFile = new BinaryFormatter();                            
                try
                {
                    des = (Data)OpenFile.Deserialize(ReadStream);                        
                }
                catch
                { }
            }

            /// <summary>
            ///从文件读取
            /// </summary>
            static public void LoadFromFile(ref Data des, string FileName)
            {
                FileStream OpenFileStream = new FileStream(FileName, FileMode.OpenOrCreate);  
                try
                {
                    LoadFromStream(ref des, OpenFileStream);
                }
                catch
                { }
                finally
                {
                    OpenFileStream.Close();                                                   
                }
            }

            /// <summary>
            ///从byte读取
            /// </summary>
            static public void LoadFromByte(ref Data des, byte[] ReadByte)
            {
                Stream SaveStream = new MemoryStream(ReadByte);      
                try
                {
                    LoadFromStream(ref des, SaveStream);
                }
                catch
                { }
                finally
                {
                    SaveStream.Close();                                                  
                }
            }

            /// <summary>
            ///从Xml读取
            /// </summary>
            static public void LoadFromXml(Type souType, ref Data sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(souType);

                FileStream stream = new FileStream(FileName, FileMode.Open);
                try
                {
                    sou = (Data)ser.Deserialize(stream);
                }
                catch
                {

                }
                finally
                {
                    stream.Close();
                }
            }

            #region IDisposable 成员

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion

            #region ICloneable 成员

            /// <summary>
            /// 克隆
            /// </summary>
            /// <returns>自身</returns>
            virtual public object Clone()
            {
                Data CloneData = Data.CopyFrom(this);
                return CloneData;
            }

            #endregion

            /// <summary>
            /// 处理资源释放的具体方法
            /// </summary>
            virtual protected void Dispose(bool disposing)
            {
                if (!Disposed)
                {
                    if (disposing)
                    {
                        //释放托管资源
                    }
                    //释放非托管资源

                }
                Disposed = true;
            }
        }

        [Serializable()]
        public class DataList : ArrayList, IZmrData
        {
            /// <summary>
            /// 标记Dispose是否被调用
            /// </summary>
            protected bool Disposed = false;

            /// <summary>
            /// 析构函数
            /// </summary>
            ~DataList()
            {
                Dispose(false);
            }

            /// <summary>
            /// 深层复制(利用缓存区对象)
            /// </summary>
            static public void CopyFrom(DataList sou, ref DataList des)
            {
                using (Stream S = new MemoryStream())
                {
                    BinaryFormatter B = new BinaryFormatter();
                    B.Serialize(S, sou);
                    S.Position = 0;
                    des = (DataList)B.Deserialize(S);
                    S.Close();
                }
            }

            /// <summary>
            ///保存到流
            /// </summary>
            static public void SaveToStream(DataList sou, Stream SaveStream)
            {
                BinaryFormatter SaveFile = new BinaryFormatter();
                SaveFile.Serialize(SaveStream, sou);
            }

            /// <summary>
            ///保存到文件
            /// </summary>
            static public void SaveToFile(DataList sou, string FileName)
            {
                FileStream SaveFileStream = new FileStream(FileName, FileMode.Create);
                try
                {
                    SaveToStream(sou, SaveFileStream);
                }
                finally
                {
                    SaveFileStream.Close();
                }
            }

            /// <summary>
            ///保存到byte
            /// </summary>
            static public void SaveToByte(DataList sou, byte[] SaveByte)
            {
                Stream SaveStream = new MemoryStream();
                try
                {
                    SaveToStream(sou, SaveStream);
                    SaveStream.Position = 0;
                    SaveStream.Read(SaveByte, 0, (int)SaveStream.Length);
                }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///保存到Xml
            /// </summary>
            static public void SaveToXml(DataList sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(sou.GetType());
                FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    ser.Serialize(stream, sou);
                }
                finally
                {
                    stream.Close();
                }
            }

            /// <summary>
            ///从流读取
            /// </summary>
            static public void LoadFromStream(ref DataList des, Stream ReadStream)
            {
                BinaryFormatter OpenFile = new BinaryFormatter();
                try
                {
                    des = (DataList)OpenFile.Deserialize(ReadStream);
                }
                catch
                { }
            }

            /// <summary>
            ///从文件读取
            /// </summary>
            static public void LoadFromFile(ref DataList des, string FileName)
            {
                FileStream OpenFileStream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    LoadFromStream(ref des, OpenFileStream);
                }
                catch
                { }
                finally
                {
                    OpenFileStream.Close();
                }
            }

            /// <summary>
            ///从byte读取
            /// </summary>
            static public void LoadFromByte(ref DataList des, byte[] ReadByte)
            {
                Stream SaveStream = new MemoryStream(ReadByte);
                try
                {
                    LoadFromStream(ref des, SaveStream);
                }
                catch
                { }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///从Xml读取
            /// </summary>
            static public void LoadFromXml(Type souType, ref DataList sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(souType);

                FileStream stream = new FileStream(FileName, FileMode.Open);
                try
                {
                    sou = (DataList)ser.Deserialize(stream);
                }
                catch
                {

                }
                finally
                {
                    stream.Close();
                }
            }

            #region IDisposable 成员

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion

            #region ICloneable 成员

            /// <summary>
            /// 克隆
            /// </summary>
            /// <returns>自身</returns>
            override public object Clone()
            {
                DataList CloneDataList = null;
                DataList.CopyFrom(this, ref CloneDataList);
                return CloneDataList;
            }

            #endregion

            /// <summary>
            /// 处理资源释放的具体方法
            /// </summary>
            virtual protected void Dispose(bool disposing)
            {
                if (!Disposed)
                {
                    if (disposing)
                    {
                        //释放托管资源

                        while (Count > 0)
                        {
                            int LastIndex = Count - 1;
                            Data ChildData = (Data)this[LastIndex];
                            ChildData.Dispose();
                            ChildData = null;
                            RemoveAt(LastIndex);
                        }
                    }
                    //释放非托管资源

                }
                Disposed = true;
            }
        }

        [Serializable()]
        public class DataListEx : List<IZmrData>, IZmrData
        {
            /// <summary>
            /// 标记Dispose是否被调用
            /// </summary>
            protected bool Disposed = false;

            /// <summary>
            /// 析构函数
            /// </summary>
            ~DataListEx()
            {
                Dispose(false);
            }

            /// <summary>
            /// 深层复制(利用缓存区对象)
            /// </summary>
            static public void CopyFrom(DataListEx sou, ref DataListEx des)
            {
                using (Stream S = new MemoryStream())
                {
                    BinaryFormatter B = new BinaryFormatter();
                    B.Serialize(S, sou);
                    S.Position = 0;
                    des = (DataListEx)B.Deserialize(S);
                    S.Close();
                }
            }

            /// <summary>
            ///保存到流
            /// </summary>
            static public void SaveToStream(DataListEx sou, Stream SaveStream)
            {
                BinaryFormatter SaveFile = new BinaryFormatter();
                SaveFile.Serialize(SaveStream, sou);
            }

            /// <summary>
            ///保存到文件
            /// </summary>
            static public void SaveToFile(DataListEx sou, string FileName)
            {
                FileStream SaveFileStream = new FileStream(FileName, FileMode.Create);
                try
                {
                    SaveToStream(sou, SaveFileStream);
                }
                finally
                {
                    SaveFileStream.Close();
                }
            }

            /// <summary>
            ///保存到byte
            /// </summary>
            static public void SaveToByte(DataListEx sou, byte[] SaveByte)
            {
                Stream SaveStream = new MemoryStream();
                try
                {
                    SaveToStream(sou, SaveStream);
                    SaveStream.Position = 0;
                    SaveStream.Read(SaveByte, 0, (int)SaveStream.Length);
                }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///保存到Xml
            /// </summary>
            static public void SaveToXml(DataListEx sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(sou.GetType());
                FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    ser.Serialize(stream, sou);
                }
                finally
                {
                    stream.Close();
                }
            }

            /// <summary>
            ///从流读取
            /// </summary>
            static public void LoadFromStream(ref DataListEx des, Stream ReadStream)
            {
                BinaryFormatter OpenFile = new BinaryFormatter();
                try
                {
                    des = (DataListEx)OpenFile.Deserialize(ReadStream);
                }
                catch
                { }
            }

            /// <summary>
            ///从文件读取
            /// </summary>
            static public void LoadFromFile(ref DataListEx des, string FileName)
            {
                FileStream OpenFileStream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    LoadFromStream(ref des, OpenFileStream);
                }
                catch
                { }
                finally
                {
                    OpenFileStream.Close();
                }
            }

            /// <summary>
            ///从byte读取
            /// </summary>
            static public void LoadFromByte(ref DataListEx des, byte[] ReadByte)
            {
                Stream SaveStream = new MemoryStream(ReadByte);
                try
                {
                    LoadFromStream(ref des, SaveStream);
                }
                catch
                { }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///从Xml读取
            /// </summary>
            static public void LoadFromXml(Type souType, ref DataListEx sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(souType);

                FileStream stream = new FileStream(FileName, FileMode.Open);
                try
                {
                    sou = (DataListEx)ser.Deserialize(stream);
                }
                catch
                {

                }
                finally
                {
                    stream.Close();
                }
            }

            #region ICloneable 成员

            public object Clone()
            {
                DataListEx CloneDataList = null;
                DataListEx.CopyFrom(this, ref CloneDataList);
                return CloneDataList;
            }

            #endregion

            #region IDisposable 成员

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion

            /// <summary>
            /// 处理资源释放的具体方法
            /// </summary>
            virtual protected void Dispose(bool disposing)
            {
                if (!Disposed)
                {
                    if (disposing)
                    {
                        //释放托管资源

                        while (Count > 0)
                        {
                            int LastIndex = Count - 1;
                            Data ChildData = (Data)this[LastIndex];
                            ChildData.Dispose();
                            ChildData = null;
                            RemoveAt(LastIndex);
                        }
                    }
                    //释放非托管资源

                }
                Disposed = true;
            }
        }

        [Serializable()]
        public class DataListNew : List<Data>
        {
            /// <summary>
            /// 标记Dispose是否被调用
            /// </summary>
            protected bool Disposed = false;

            /// <summary>
            /// 析构函数
            /// </summary>
            ~DataListNew()
            {
                Dispose(false);
            }

            /// <summary>
            /// 深层复制(利用缓存区对象)
            /// </summary>
            static public void CopyFrom(DataListNew sou, ref DataListNew des)
            {
                using (Stream S = new MemoryStream())
                {
                    BinaryFormatter B = new BinaryFormatter();
                    B.Serialize(S, sou);
                    S.Position = 0;
                    des = (DataListNew)B.Deserialize(S);
                    S.Close();
                }
            }

            /// <summary>
            ///保存到流
            /// </summary>
            static public void SaveToStream(DataListNew sou, Stream SaveStream)
            {
                BinaryFormatter SaveFile = new BinaryFormatter();
                SaveFile.Serialize(SaveStream, sou);
            }

            /// <summary>
            ///保存到文件
            /// </summary>
            static public void SaveToFile(DataListNew sou, string FileName)
            {
                FileStream SaveFileStream = new FileStream(FileName, FileMode.Create);
                try
                {
                    SaveToStream(sou, SaveFileStream);
                }
                finally
                {
                    SaveFileStream.Close();
                }
            }

            /// <summary>
            ///保存到byte
            /// </summary>
            static public void SaveToByte(DataListNew sou, byte[] SaveByte)
            {
                Stream SaveStream = new MemoryStream();
                try
                {
                    SaveToStream(sou, SaveStream);
                    SaveStream.Position = 0;
                    SaveStream.Read(SaveByte, 0, (int)SaveStream.Length);
                }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///保存到Xml
            /// </summary>
            static public void SaveToXml(DataListNew sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(sou.GetType());
                FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    ser.Serialize(stream, sou);
                }
                finally
                {
                    stream.Close();
                }
            }

            /// <summary>
            ///从流读取
            /// </summary>
            static public void LoadFromStream(ref DataListNew des, Stream ReadStream)
            {
                BinaryFormatter OpenFile = new BinaryFormatter();
                try
                {
                    des = (DataListNew)OpenFile.Deserialize(ReadStream);
                }
                catch
                { }
            }

            /// <summary>
            ///从文件读取
            /// </summary>
            static public void LoadFromFile(ref DataListNew des, string FileName)
            {
                FileStream OpenFileStream = new FileStream(FileName, FileMode.OpenOrCreate);
                try
                {
                    LoadFromStream(ref des, OpenFileStream);
                }
                catch
                { }
                finally
                {
                    OpenFileStream.Close();
                }
            }

            /// <summary>
            ///从byte读取
            /// </summary>
            static public void LoadFromByte(ref DataListNew des, byte[] ReadByte)
            {
                Stream SaveStream = new MemoryStream(ReadByte);
                try
                {
                    LoadFromStream(ref des, SaveStream);
                }
                catch
                { }
                finally
                {
                    SaveStream.Close();
                }
            }

            /// <summary>
            ///从Xml读取
            /// </summary>
            static public void LoadFromXml(Type souType, ref DataListNew sou, string FileName)
            {
                XmlSerializer ser = new XmlSerializer(souType);

                FileStream stream = new FileStream(FileName, FileMode.Open);
                try
                {
                    sou = (DataListNew)ser.Deserialize(stream);
                }
                catch
                {

                }
                finally
                {
                    stream.Close();
                }
            }

            #region ICloneable 成员

            public object Clone()
            {
                DataListNew CloneDataList = null;
                DataListNew.CopyFrom(this, ref CloneDataList);
                return CloneDataList;
            }

            #endregion

            #region IDisposable 成员

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion

            /// <summary>
            /// 处理资源释放的具体方法
            /// </summary>
            virtual protected void Dispose(bool disposing)
            {
                if (!Disposed)
                {
                    if (disposing)
                    {
                        //释放托管资源

                        while (Count > 0)
                        {
                            int LastIndex = Count - 1;
                            Data ChildData = (Data)this[LastIndex];
                            ChildData.Dispose();
                            ChildData = null;
                            RemoveAt(LastIndex);
                        }
                    }
                    //释放非托管资源

                }
                Disposed = true;
            }
        }
}
}
