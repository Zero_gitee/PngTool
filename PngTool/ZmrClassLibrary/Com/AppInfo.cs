using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace ZmrSoft
{
    /// <summary>
    /// 程序信息
    /// </summary>
    [Serializable()]
    public class AppInfo : Data.DataList, IXmlSerializable
    {
        private string _AppGuid;
        private string _AppName;
        private string _Remark;
        private DateTime _PublishedTime;

        public AppInfo()
        {
            _AppGuid = Guid.NewGuid().ToString();
            _PublishedTime = DateTime.Now;
        }

        /// <summary>
        /// 应用程序ID
        /// </summary>
        public string AppGuid
        {
            get
            {
                return _AppGuid;
            }
            set
            {
                _AppGuid = value;
            }
        }

        /// <summary>
        /// 应用程序名称
        /// </summary>
        public string AppName
        {
            get
            {
                return _AppName;
            }
            set
            {
                _AppName = value;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                _Remark = value;
            }
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime PublishedTime
        {
            get
            {
                return _PublishedTime;
            }
            set
            {
                _PublishedTime = value;
            }
        }

        /// <summary>
        /// 最新版本信息
        /// </summary>
        public VersionInfo NewVersion
        {
            get
            {
                VersionInfo Ret = null;

                int Index = Count - 1;
                if (Index > -1)
                {
                    Ret = this[Index] as VersionInfo;
                }

                return Ret;
            }
        }

        #region IXmlSerializable 成员

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "AppInfo")
            {
                _AppGuid = reader["AppGuid"];
                _AppName = reader["AppName"];
                _Remark = reader["Remark"];
                _PublishedTime = DateTime.FromBinary(Int64.Parse(reader["PublishedTime"]));
                if (reader.ReadToDescendant("VersionInfo"))
                {
                    while (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "VersionInfo")
                    {
                        VersionInfo vi = new VersionInfo();
                        vi.ReadXml(reader);
                        this.Add(vi);
                    }
                }
                reader.Read();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("AppGuid", _AppGuid);
            writer.WriteAttributeString("AppName", _AppName);
            writer.WriteAttributeString("Remark", _Remark);
            writer.WriteAttributeString("PublishedTime", _PublishedTime.ToBinary().ToString());
            foreach (VersionInfo vi in this)
            {
                writer.WriteStartElement("VersionInfo");
                vi.WriteXml(writer);
                writer.WriteEndElement();
            } 
        }

        #endregion
    }
}
