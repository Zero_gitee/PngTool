using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace ZmrSoft
{
    /// <summary>
    /// 程序信息列表
    /// </summary>
    [Serializable()]
    public class AppInfoList : Data.DataList, IXmlSerializable
    {
        #region IXmlSerializable 成员

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "AppInfoList")
            {
                if (reader.ReadToDescendant("AppInfo"))
                {
                    while (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "AppInfo")
                    {
                        AppInfo ai = new AppInfo();
                        ai.ReadXml(reader);
                        this.Add(ai);
                    }
                }
                reader.Read();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            foreach (AppInfo ai in this)
            {
                writer.WriteStartElement("AppInfo");
                ai.WriteXml(writer);
                writer.WriteEndElement();
            } 
        }

        #endregion

        static public AppInfoList LoadFromXmlFile(string FileName)
        {
            AppInfoList Ret = null;
            XmlSerializer xs = new XmlSerializer(typeof(AppInfoList));
            Stream stream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            try
            {
                Ret = (AppInfoList)xs.Deserialize(stream);
            }
            finally
            {
                stream.Close();
            }

            return Ret;
        }

        static public AppInfoList LoadFromXmlUrl(string Url)
        {
            AppInfoList Ret = null;
            XmlSerializer xs = new XmlSerializer(typeof(AppInfoList));
            XmlReader Reader = XmlReader.Create(Url);  
            try
            {
                Ret = (AppInfoList)xs.Deserialize(Reader);
            }
            finally
            {
                Reader.Close();
            }

            return Ret;
        }

        static public void SaveToXmlFile(AppInfoList Sou, string FileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(AppInfoList));
            Stream stream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            try
            {
                xs.Serialize(stream, Sou);
            }
            finally
            {
                stream.Close();
            }
        }
    }
}
