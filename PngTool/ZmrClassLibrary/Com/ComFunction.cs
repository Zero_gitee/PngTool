﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ZmrSoft.Data;
using System.Windows.Forms;

namespace ZmrSoft
{
    public class ComFunction
    {
        //下载文件
        static public bool UpdateFile(string Url, string LocalFileName)
        {
            bool Ret = false;

            //新建文件
            if (File.Exists(LocalFileName))
            {
                File.Delete(LocalFileName);
            }
            FileStream fs = new FileStream(LocalFileName, FileMode.Create);

            //打开网络连接
            try
            {
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(Url);
                long l = request.GetResponse().ContentLength; //文件大小
                Stream ns = request.GetResponse().GetResponseStream();

                byte[] nbytes = new byte[512];
                int nReadSize = 0;
                nReadSize = ns.Read(nbytes, 0, 512);

                while (nReadSize > 0)
                {
                    fs.Write(nbytes, 0, nReadSize);
                    nReadSize = ns.Read(nbytes, 0, 512);
                }
                fs.Close();
                ns.Close();

                Ret = true;
            }
            catch
            {
                fs.Close();
            }

            return Ret;
        }

        //取当前软件的最新版本
        static public VersionInfo GetNewVersionInfo()
        {
            VersionInfo Ret = null;

            //下载文件
            string StrUrl = @"http://files.cnblogs.com/zmrbj/ZmrSoft.sil.rar";
            string StrFileName = Application.StartupPath + @"\ZmrSoft.sil";

            if (ZmrSoft.ComFunction.UpdateFile(StrUrl, StrFileName))
            {
                DataList NewDataList = new DataList();
                DataList.LoadFromFile(ref NewDataList, StrFileName);
                AppInfoList NewAppInfoList = NewDataList as AppInfoList;

                foreach (AppInfo aInfo in NewAppInfoList)
                {
                    if (aInfo.AppName == Application.ProductName)
                    {
                        Ret = aInfo.NewVersion;
                        break;
                    }
                }
            }

            if (File.Exists(StrFileName))
            {
                File.Delete(StrFileName);
            }

            return Ret;
        }

        //通过Xml取当前软件的最新版本
        static public VersionInfo GetNewVersionInfoByXml()
        {
            VersionInfo Ret = null;

            //下载文件
            string StrUrl = @"http://files.cnblogs.com/zmrbj/AppInfoList.xml";
            string StrFileName = Application.StartupPath + @"\AppInfoList.xml";

            if (ZmrSoft.ComFunction.UpdateFile(StrUrl, StrFileName))
            {
                AppInfoList NewAppInfoList = AppInfoList.LoadFromXmlFile(StrFileName);

                foreach (AppInfo aInfo in NewAppInfoList)
                {
                    if (aInfo.AppName == Application.ProductName)
                    {
                        Ret = aInfo.NewVersion;
                        break;
                    }
                }
            }

            if (File.Exists(StrFileName))
            {
                File.Delete(StrFileName);
            }

            return Ret;
        }

        //通过XmlUrl取当前软件的最新版本
        static public VersionInfo GetNewVersionInfoByXmlUrl()
        {
            VersionInfo Ret = null;

            string StrUrl = @"http://files.cnblogs.com/zmrbj/AppInfoList.xml";
            AppInfoList NewAppInfoList = AppInfoList.LoadFromXmlUrl(StrUrl);

            foreach (AppInfo aInfo in NewAppInfoList)
            {
                if (aInfo.AppName == Application.ProductName)
                {
                    Ret = aInfo.NewVersion;
                    break;
                }
            }

            return Ret;
        }

        //打开链接
        static public void OpenLink(string Url)
        {
            //string DefBrowser = "";

            ////读取注册表，得到默认浏览器
            //try
            //{
            //    Microsoft.Win32.RegistryKey RegMain = Microsoft.Win32.Registry.ClassesRoot;
            //    Microsoft.Win32.RegistryKey RegChild = RegMain.OpenSubKey(@"http\shell\open\command");
            //    DefBrowser = (string)RegChild.GetValue("");
            //}
            //catch
            //{

            //}

            ////使用Postbuild2007以后不支持Google浏览器，改为IE(权宜之计)
            //if (DefBrowser.IndexOf("chrome.exe") > 0)
            //{
            //    System.Diagnostics.Process.Start("iexplore.exe", Url);
            //}
            //else
            //{
            //    System.Diagnostics.Process.Start(Url);
            //}

            System.Diagnostics.Process.Start(Url);
        }

        //判断是否需要升级
        static private bool IsCheckUpdate = false;
        public static void CheckUpdate(bool NeedMsg)
        {
            if (IsCheckUpdate) return;
            IsCheckUpdate = true;

            bool Had = false;

            VersionInfo NewVersionInfo = null;
            try
            {
                NewVersionInfo = GetNewVersionInfo();
            }
            catch
            {
                if (NeedMsg)
                {
                    MessageBox.Show(@"检查更新失败！请检查网络是否正常。
若无法解决，请参考作者博客http://baijing-no.11.blog.163.com/");
                }
                return;
            }

            if (NewVersionInfo != null)
            {
                Version OldVersion = new Version(Application.ProductVersion);
                Version NewVersion = new Version(NewVersionInfo.VersionNum);

                if (NewVersion > OldVersion)
                {
                    Had = true;

                    string Msg = "最新版本：" + NewVersionInfo.VersionName + @"
版 本 号：" + NewVersionInfo.VersionNum + @"
发布时间：" + NewVersionInfo.PublishedTime.ToShortDateString() + @"
更新内容：" + NewVersionInfo.Remark;

                    Msg += @"
是否立即到作者博客下载？";
                    if (MessageBox.Show(Msg, Application.ProductName + "有更新！", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ZmrSoft.ComFunction.OpenLink(NewVersionInfo.BlogUrl);
                    }
                }
            }

            if ((!Had) && (NeedMsg))
            {
                MessageBox.Show("当前版本已经是最新！");
            }

            IsCheckUpdate = false;
        }

        public static void CheckUpdateByXml(bool NeedMsg)
        {
            if (IsCheckUpdate) return;
            IsCheckUpdate = true;

            bool Had = false;

            VersionInfo NewVersionInfo = null;
            try
            {
                NewVersionInfo = GetNewVersionInfoByXml();
            }
            catch
            {
                if (NeedMsg)
                {
                    MessageBox.Show(@"检查更新失败！请检查网络是否正常。
若无法解决，请参考作者博客http://baijing-no.11.blog.163.com/");
                }
                return;
            }

            if (NewVersionInfo != null)
            {
                Version OldVersion = new Version(Application.ProductVersion);
                Version NewVersion = new Version(NewVersionInfo.VersionNum);

                if (NewVersion > OldVersion)
                {
                    Had = true;

                    string Msg = "最新版本：" + NewVersionInfo.VersionName + @"
版 本 号：" + NewVersionInfo.VersionNum + @"
发布时间：" + NewVersionInfo.PublishedTime.ToShortDateString() + @"
更新内容：" + NewVersionInfo.Remark;

                    Msg += @"
是否立即到作者博客下载？";
                    if (MessageBox.Show(Msg, Application.ProductName + "有更新！", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ZmrSoft.ComFunction.OpenLink(NewVersionInfo.BlogUrl);
                    }
                }
            }

            if ((!Had) && (NeedMsg))
            {
                MessageBox.Show("当前版本已经是最新！");
            }

            IsCheckUpdate = false;
        }
        public static void CheckUpdateByXmlUrl(bool NeedMsg)
        {
            if (IsCheckUpdate) return;
            IsCheckUpdate = true;

            bool Had = false;

            VersionInfo NewVersionInfo = null;
            try
            {
                NewVersionInfo = GetNewVersionInfoByXmlUrl();
            }
            catch
            {
                if (NeedMsg)
                {
                    MessageBox.Show(@"检查更新失败！请检查网络是否正常。
若无法解决，请参考作者博客http://baijing-no.11.blog.163.com/");
                }
                return;
            }

            if (NewVersionInfo != null)
            {
                Version OldVersion = new Version(Application.ProductVersion);
                Version NewVersion = new Version(NewVersionInfo.VersionNum);

                if (NewVersion > OldVersion)
                {
                    Had = true;

                    string Msg = "最新版本：" + NewVersionInfo.VersionName + @"
版 本 号：" + NewVersionInfo.VersionNum + @"
发布时间：" + NewVersionInfo.PublishedTime.ToShortDateString() + @"
更新内容：" + NewVersionInfo.Remark;

                    Msg += @"
是否立即到作者博客下载？";
                    if (MessageBox.Show(Msg, Application.ProductName + "有更新！", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ZmrSoft.ComFunction.OpenLink(NewVersionInfo.BlogUrl);
                    }
                }
            }

            if ((!Had) && (NeedMsg))
            {
                MessageBox.Show("当前版本已经是最新！");
            }

            IsCheckUpdate = false;
        }
    }
}
