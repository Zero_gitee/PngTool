using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ZmrSoft
{
    public class ControlManage : List<Control>
    {
        static private ControlManage _Manage = null;

        static public void RegisterControl(Control Ctrl)
        {
            if (_Manage == null)
            {
                _Manage = new ControlManage();
            }

            if (!_Manage.FindControl(Ctrl))
            {
                _Manage.Add(Ctrl);
            }
        }

        static public void UnRegisterControl(Control Ctrl)
        {
            if (_Manage != null)
            {
                if (_Manage.FindControl(Ctrl))
                {
                    _Manage.Remove(Ctrl);
                }
            }
        }

        static public void SendMassage(int msg, int wParam, int lParam)
        {
            if (_Manage != null)
            {
                foreach (Control c in _Manage)
                {
                    ZmrSoft.WinAPI.Win32API.SendMessage(c.Handle, msg, wParam, lParam);
                }
            }
        }

        static public void SendMassage(string ControlName, int msg, int wParam, int lParam)
        {
            if (_Manage != null)
            {
                foreach (Control c in _Manage)
                {
                    if (c.Name == ControlName)
                    {
                        ZmrSoft.WinAPI.Win32API.SendMessage(c.Handle, msg, wParam, lParam);
                    }
                }
            }
        }

        private ControlManage()
        {

        }

        private bool FindControl(Control Ctrl)
        {
            bool Ret = false;

            foreach (Control c in this)
            {
                if (c.Equals(Ctrl))
                {
                    Ret = true;
                    break;
                }
            }

            return Ret;
        }
    }
}
