using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ZmrSoft
{
    /// <summary>
    /// 版本信息
    /// </summary>
    [Serializable()]
    public class VersionInfo : Data.Data, IXmlSerializable
    {
        private string _VersionNum;
        private string _Remark;
        private DateTime _PublishedTime;
        private string _VersionName;
        private string _BlogUrl;
        private string _UpdateUrl;

        public VersionInfo()
        {
            _PublishedTime = DateTime.Now;
            _BlogUrl = @"http://baijing-no.11.blog.163.com/";
        }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishedTime
        {
            get
            {
                return _PublishedTime;
            }
            set
            {
                _PublishedTime = value;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                _Remark = value;
            }
        }

        /// <summary>
        /// 版本名称
        /// </summary>
        public string VersionName
        {
            get
            {
                return _VersionName;
            }
            set
            {
                _VersionName = value;
            }
        }

        /// <summary>
        /// 版本号
        /// </summary>
        public string VersionNum
        {
            get
            {
                return _VersionNum;
            }
            set
            {
                _VersionNum = value;
            }
        }

        /// <summary>
        /// 博客相关文章链接
        /// </summary>
        public string BlogUrl
        {
            get
            {
                return _BlogUrl;
            }
            set
            {
                _BlogUrl = value;
            }
        }

        /// <summary>
        /// 下载链接
        /// </summary>
        public string UpdateUrl
        {
            get
            {
                return _UpdateUrl;
            }
            set
            {
                _UpdateUrl = value;
            }
        }

        #region IXmlSerializable 成员

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            _VersionNum = reader["VersionNum"];
            _Remark = reader["Remark"];
            _PublishedTime = DateTime.FromBinary(Int64.Parse(reader["PublishedTime"]));
            _VersionName = reader["VersionName"];
            _BlogUrl = reader["BlogUrl"];
            _UpdateUrl = reader["UpdateUrl"];

            reader.Read();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("VersionNum", _VersionNum);
            writer.WriteAttributeString("Remark", _Remark);
            writer.WriteAttributeString("PublishedTime", _PublishedTime.ToBinary().ToString()); 
            writer.WriteAttributeString("VersionName", _VersionName);
            writer.WriteAttributeString("BlogUrl", _BlogUrl);
            writer.WriteAttributeString("UpdateUrl", _UpdateUrl);
        }

        #endregion
    }
}
