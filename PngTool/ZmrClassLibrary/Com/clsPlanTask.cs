using System;
using System.Collections.Generic;
using System.Text;

namespace ZmrSoft.ZmrSystem
{
    /// <summary>
    /// 计划任务操作类
    /// </summary>
    public class clsPlanTask
    {
        private string _TaskRun;
        private string _TaskName;
        private string _Param;
        private enumTaskType _TaskType;
        private clsTaskType _Schedule;
        private string _Domain;
        private string _PassWord;
        private string _User;

        public clsPlanTask()
        {
            _Domain = System.Environment.UserDomainName;
            _User = System.Environment.UserName;
        }

        /// <summary>
        /// 任务的名称
        /// </summary>
        public string TaskName
        {
            get
            {
                return _TaskName;
            }
            set
            {
                _TaskName = value;
            }
        }

        /// <summary>
        /// 运行的程序或命令
        /// </summary>
        /// <remarks>可执行文件、脚本文件或批处理文件的完全合格的路径和文件名。如果忽略该路径，SchTasks.exe 将假定文件在 Systemroot\System32 目录下。</remarks>
        public string TaskRun
        {
            get
            {
                return _TaskRun;
            }
            set
            {
                _TaskRun = value;
            }
        }

        /// <summary>
        /// 参数
        /// </summary>
        public string Param
        {
            get
            {
                return _Param;
            }
            set
            {
                _Param = value;
            }
        }

        /// <summary>
        /// 计划类型
        /// </summary>
        /// <remarks>有效值为 MINUTE、HOURLY、DAILY、WEEKLY、MONTHLY、ONCE、ONSTART、ONLOGON、ONIDLE。</remarks>
        public enumTaskType TaskType
        {
            get
            {
                return _TaskType;
            }
            set
            {
                _TaskType = value;
                _Schedule = clsTaskType.CreateTaskType(value);
            }
        }

        /// <summary>
        /// 用户名
        /// </summary>
        public string User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string PassWord
        {
            get
            {
                return _PassWord;
            }
            set
            {
                _PassWord = value;
            }
        }

        /// <summary>
        /// 用户域
        /// </summary>
        public string Domain
        {
            get
            {
                return _Domain;
            }
            set
            {
                _Domain = value;
            }
        }

        /// <summary>
        /// 获取Dos命令字符串
        /// </summary>
        public string GetDosCommand()
        {
            string Ret = "";

            if (_Schedule == null)
            {
                return Ret;
            }
            else
            {
                Ret += @" schtasks /create";
                Ret += @" /tn " + TaskName;
                Ret += " /tr \"" + TaskRun + " " + Param + "\" ";
                Ret += _Schedule.GetCommand();
                Ret += @" /ru " + Domain + @"\" + User;
                Ret += @" /rp zmrlyc";
            }

            return Ret;
        }

        /// <summary>
        /// 添加计划任务
        /// </summary>
        /// <param name="NewPlanTask">要添加的计划任务</param>
        public static int AddPlanTask(clsPlanTask NewPlanTask)
        {
            string DosCommand = NewPlanTask.GetDosCommand();
            System.Windows.Forms.MessageBox.Show(DosCommand);

            return 0;
        }

        /// <summary>
        /// 删除计划任务
        /// </summary>
        /// <param name="PlanTask">要删除的计划任务</param>
        public static int DelPlanTask(clsPlanTask PlanTask)
        {
            throw new System.NotImplementedException();
        }
    }

    /// <summary>
    /// 计划任务类型
    /// </summary>
    public enum enumTaskType
    {
        /// <summary>
        /// 以分钟
        /// </summary>
        MINUTE,
        HOURLY,
        DAILY,
        WEEKLY,
        MONTHLY,
        ONCE,
        ONSTART,
        ONLOGON,
        ONIDLE,
    }
}
