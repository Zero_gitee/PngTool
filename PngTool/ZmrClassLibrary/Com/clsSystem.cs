﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using Microsoft.Win32;

namespace ZmrSoft
{
    namespace ZmrSystem
    {
        public class clsSystem
        {
            #region 运行可执行程序，通常用于带参数的命令行(operType一般为5(SW_NORMAL))

            [DllImport("kernel32.dll")]
            public static extern int WinExec(string exeName, int operType);

            #endregion

            #region 取进程用户名(传入参数:进程名;返回值:如"计算机名/用户名")

            public static string GetProgramUserName(string ProgramName)
            {
                string Ret = "";

                ConnectionOptions oConn = new ConnectionOptions();
                System.Management.ManagementScope oMs = new System.Management.ManagementScope("\\\\localhost", oConn);

                //get   Process   objects  
                System.Management.ObjectQuery oQuery = new System.Management.ObjectQuery("Select * from Win32_Process where Name = '" + ProgramName + ".exe'");
                ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oMs, oQuery);
                ManagementObjectCollection oReturnCollection = oSearcher.Get();

                foreach (ManagementObject oReturn in oReturnCollection)
                {
                    string[] o = new String[2];

                    oReturn.InvokeMethod("GetOwner", (object[])o);

                    Ret = o[1] + "\\" + o[0];
                }

                return Ret;
            }

            #endregion

            #region 重建图标缓存

            [DllImport("User32.dll", EntryPoint = "SendMessage")]
            private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

            public const int WM_SETTINGCHANGE = 26;
            public const int HWND_BROADCAST = 65535;


            //指定窗口
            [DllImport("User32.dll", EntryPoint = "FindWindow")]
            private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

            public static void ReBuildIconCache()
            {
                IntPtr hDeskTop = FindWindow("Progman", "Program Manager"); 

                int value = Convert.ToInt32(Registry.GetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop\WindowMetrics", "Shell Icon Size", 32).ToString());
                Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop\WindowMetrics", "Shell Icon Size", value + 1, RegistryValueKind.String);
                //SendMessage(new IntPtr(HWND_BROADCAST), WM_SETTINGCHANGE, 0, 0); Edit By 2009.12.29：不用广播消息，给桌面发就行了
                SendMessage(hDeskTop, WM_SETTINGCHANGE, 0, 0);
                Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop\WindowMetrics", "Shell Icon Size", value, RegistryValueKind.String);
                //SendMessage(new IntPtr(HWND_BROADCAST), WM_SETTINGCHANGE, 0, 0);
                SendMessage(hDeskTop, WM_SETTINGCHANGE, 0, 0);
            }

            #endregion
        }

        //Ini文件操作
        public class INIClass
        {
            public string inipath;
            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
            /// <summary>
            /// 构造方法
            /// </summary>
            /// <param name="INIPath">文件路径</param>
            public INIClass(string INIPath)
            {
                inipath = INIPath;
            }
            /// <summary>
            /// 写入INI文件
            /// </summary>
            /// <param name="Section">项目名称(如 [TypeName] )</param>
            /// <param name="Key">键</param>
            /// <param name="Value">值</param>
            public void IniWriteValue(string Section, string Key, string Value)
            {
                WritePrivateProfileString(Section, Key, Value, this.inipath);
            }
            /// <summary>
            /// 读出INI文件
            /// </summary>
            /// <param name="Section">项目名称(如 [TypeName] )</param>
            /// <param name="Key">键</param>
            public string IniReadValue(string Section, string Key)
            {
                StringBuilder temp = new StringBuilder(500);
                int i = GetPrivateProfileString(Section, Key, "", temp, 500, this.inipath);
                return temp.ToString();
            }
            /// <summary>
            /// 验证文件是否存在
            /// </summary>
            /// <returns>布尔值</returns>
            public bool ExistINIFile()
            {
                return File.Exists(inipath);
            }
        }

        //读取指定程序集信息
        public class GetAssemblyInfo
        {
            //要读取的程序集
            private Assembly CurAss;

            public GetAssemblyInfo(Assembly Ass)
            {
                CurAss = Ass;
            }

            //读取指定信息
            public Attribute GetAttribute(Type AttributeType)
            {
                Attribute Ret = null;
                foreach (Attribute attr in CurAss.GetCustomAttributes(true))
                {
                    if (attr.GetType() == AttributeType)
                    {
                        Ret = attr;
                        break;
                    }
                }
                return Ret;
            }
        }
    }
}
