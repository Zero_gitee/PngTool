using System;
using System.Collections.Generic;
using System.Text;

namespace ZmrSoft.ZmrSystem
{
    public abstract class clsTaskType
    {
        private DateTime _RunDateTime;

        public clsTaskType()
        {
            _RunDateTime = DateTime.Now;
        }

        /// <summary>
        /// 运行时间
        /// </summary>
        public DateTime RunDateTime
        {
            get
            {
                return _RunDateTime;
            }
            set
            {
                _RunDateTime = value;
            }
        }
        /// <summary>
        /// 取命令字符串
        /// </summary>
        public abstract string GetCommand();

        public static clsTaskType CreateTaskType(enumTaskType TaskType)
        {
            clsTaskType Ret = null;

            switch (TaskType)
            {
                case enumTaskType.ONCE:
                    {
                        Ret = new clsOnce();
                        break;
                    }
                case enumTaskType.ONIDLE:
                    {
                        Ret = new clsOnIdle();
                        break;
                    }
                case enumTaskType.ONLOGON:
                    {
                        Ret = new clsOnLogon();
                        break;
                    }
                case enumTaskType.ONSTART:
                    {
                        Ret = new clsOnStart();
                        break;
                    }
                default:
                    {
                        Ret = new clsOnInterval(TaskType);
                        break;
                    }
            }

            return Ret;
        }
    }

    /// <summary>
    /// 启动时运行
    /// </summary>
    public class clsOnStart : clsTaskType
    {
        public override string GetCommand()
        {
            string Ret = "";

            Ret += @" /sc ONSTART";

            return Ret;
        }
    }

    /// <summary>
    /// 登录时运行
    /// </summary>
    public class clsOnLogon : clsTaskType
    {
        public override string GetCommand()
        {
            string Ret = "";

            Ret += @" /sc ONLOGON";

            return Ret;
        }
    }

    /// <summary>
    /// 间隔...运行
    /// </summary>
    /// <remarks>
    /// MINUTE 1 ～ 1439 任务每 n 分钟运行一次。 
    /// HOURLY 1 ～ 23 任务每 n 小时运行一次。 
    /// DAILY 1 ～ 365 任务每 n 天运行一次。 
    /// WEEKLY 1 ～ 52 任务每 n 周运行一次。 
    /// MONTHLY 1 ～ 12 任务每 n 月运行一次。
    /// </remarks>
    public class clsOnInterval : clsTaskType
    {
        public clsOnInterval(enumTaskType TaskType)
        {
            throw new System.NotImplementedException();
        }
    
        public override string GetCommand()
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    /// <summary>
    /// 运行一次
    /// </summary>
    public class clsOnce : clsTaskType
    {
        public override string GetCommand()
        {
            string Ret = "";

            Ret += @" /sc ONCE";
            Ret += @" /st " + RunDateTime.ToShortTimeString();
            Ret += @" /sd " + RunDateTime.ToShortDateString();

            return Ret;
        }
    }

    /// <summary>
    /// 空闲时运行
    /// </summary>
    public class clsOnIdle : clsTaskType
    {
        public override string GetCommand()
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
