﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZmrSoft
{
    namespace WinForm
    {
        public partial class frmAbout : Form
        {
            private PictureBox pictureBox;
            private Label label1;
            private Label label2;
            private Label label3;
            private Label label4;
            private Label label5;
            private TextBox textBox1;
            private LinkLabel labEmail;
            private Label labAppName;
            private Label labVersion;
            private LinkLabel labCompany;
            private Button btnOK;


            //公司链接
            private string CompanyLink = @"http://baijing-no.11.blog.163.com/";
            private ToolTip toolTip;
            private IContainer components;
            //邮箱链接
            private string EmailLink = "Soft.Zmr@Gmail.com";

            static private frmAbout frm = null;
            static private bool IsShow = false;

            private frmAbout()
            {
                InitializeComponent();
                labEmail.Text = EmailLink;
            }

            public static void ShowAboutForm()
            {
                if (IsShow)
                {
                    frm.WindowState = FormWindowState.Normal;
                    frm.Activate();
                    return;
                }
                else
                {
                    frm = new frmAbout();
                    frm.labAppName.Text = Application.ProductName;
                    frm.labVersion.Text = Application.ProductVersion;
                    frm.labCompany.Text = Application.CompanyName;

                    frm.Show();

                    IsShow = true;
                }
            }

            private void InitializeComponent()
            {
                this.components = new System.ComponentModel.Container();
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
                this.pictureBox = new System.Windows.Forms.PictureBox();
                this.btnOK = new System.Windows.Forms.Button();
                this.label1 = new System.Windows.Forms.Label();
                this.label2 = new System.Windows.Forms.Label();
                this.label3 = new System.Windows.Forms.Label();
                this.label4 = new System.Windows.Forms.Label();
                this.label5 = new System.Windows.Forms.Label();
                this.textBox1 = new System.Windows.Forms.TextBox();
                this.labEmail = new System.Windows.Forms.LinkLabel();
                this.labAppName = new System.Windows.Forms.Label();
                this.labVersion = new System.Windows.Forms.Label();
                this.labCompany = new System.Windows.Forms.LinkLabel();
                this.toolTip = new System.Windows.Forms.ToolTip(this.components);
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
                this.SuspendLayout();
                // 
                // pictureBox
                // 
                this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
                this.pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.BackgroundImage")));
                this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                this.pictureBox.Location = new System.Drawing.Point(252, 12);
                this.pictureBox.Name = "pictureBox";
                this.pictureBox.Size = new System.Drawing.Size(128, 128);
                this.pictureBox.TabIndex = 0;
                this.pictureBox.TabStop = false;
                this.toolTip.SetToolTip(this.pictureBox, "访问作者主页");
                this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click);
                // 
                // btnOK
                // 
                this.btnOK.Location = new System.Drawing.Point(305, 233);
                this.btnOK.Name = "btnOK";
                this.btnOK.Size = new System.Drawing.Size(75, 23);
                this.btnOK.TabIndex = 1;
                this.btnOK.Text = "确定";
                this.btnOK.UseVisualStyleBackColor = true;
                this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
                // 
                // label1
                // 
                this.label1.AutoSize = true;
                this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                this.label1.Location = new System.Drawing.Point(12, 12);
                this.label1.Name = "label1";
                this.label1.Size = new System.Drawing.Size(57, 12);
                this.label1.TabIndex = 2;
                this.label1.Text = "程序名称";
                // 
                // label2
                // 
                this.label2.AutoSize = true;
                this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                this.label2.Location = new System.Drawing.Point(12, 41);
                this.label2.Name = "label2";
                this.label2.Size = new System.Drawing.Size(57, 12);
                this.label2.TabIndex = 3;
                this.label2.Text = "当前版本";
                // 
                // label3
                // 
                this.label3.AutoSize = true;
                this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                this.label3.Location = new System.Drawing.Point(12, 70);
                this.label3.Name = "label3";
                this.label3.Size = new System.Drawing.Size(57, 12);
                this.label3.TabIndex = 4;
                this.label3.Text = "技术支持";
                // 
                // label4
                // 
                this.label4.AutoSize = true;
                this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                this.label4.Location = new System.Drawing.Point(12, 99);
                this.label4.Name = "label4";
                this.label4.Size = new System.Drawing.Size(47, 12);
                this.label4.TabIndex = 5;
                this.label4.Text = "E-mail";
                // 
                // label5
                // 
                this.label5.AutoSize = true;
                this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                this.label5.Location = new System.Drawing.Point(12, 128);
                this.label5.Name = "label5";
                this.label5.Size = new System.Drawing.Size(57, 12);
                this.label5.TabIndex = 6;
                this.label5.Text = "重要声明";
                // 
                // textBox1
                // 
                this.textBox1.Location = new System.Drawing.Point(12, 146);
                this.textBox1.Multiline = true;
                this.textBox1.Name = "textBox1";
                this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
                this.textBox1.Size = new System.Drawing.Size(368, 81);
                this.textBox1.TabIndex = 7;
                this.textBox1.Text = resources.GetString("textBox1.Text");
                // 
                // labEmail
                // 
                this.labEmail.AutoSize = true;
                this.labEmail.Location = new System.Drawing.Point(87, 99);
                this.labEmail.Name = "labEmail";
                this.labEmail.Size = new System.Drawing.Size(113, 12);
                this.labEmail.TabIndex = 8;
                this.labEmail.TabStop = true;
                this.labEmail.Text = "Soft.Zmr@Gmail.com";
                this.labEmail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labEmail_LinkClicked);
                // 
                // labAppName
                // 
                this.labAppName.AutoSize = true;
                this.labAppName.Location = new System.Drawing.Point(87, 12);
                this.labAppName.Name = "labAppName";
                this.labAppName.Size = new System.Drawing.Size(47, 12);
                this.labAppName.TabIndex = 9;
                this.labAppName.Text = "AppName";
                // 
                // labVersion
                // 
                this.labVersion.AutoSize = true;
                this.labVersion.Location = new System.Drawing.Point(87, 41);
                this.labVersion.Name = "labVersion";
                this.labVersion.Size = new System.Drawing.Size(47, 12);
                this.labVersion.TabIndex = 10;
                this.labVersion.Text = "Version";
                // 
                // labCompany
                // 
                this.labCompany.AutoSize = true;
                this.labCompany.Location = new System.Drawing.Point(87, 70);
                this.labCompany.Name = "labCompany";
                this.labCompany.Size = new System.Drawing.Size(47, 12);
                this.labCompany.TabIndex = 11;
                this.labCompany.TabStop = true;
                this.labCompany.Text = "ZmrSoft";
                this.labCompany.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labCompany_LinkClicked);
                // 
                // frmAbout
                // 
                this.ClientSize = new System.Drawing.Size(392, 268);
                this.Controls.Add(this.labCompany);
                this.Controls.Add(this.labVersion);
                this.Controls.Add(this.labAppName);
                this.Controls.Add(this.labEmail);
                this.Controls.Add(this.textBox1);
                this.Controls.Add(this.label5);
                this.Controls.Add(this.label4);
                this.Controls.Add(this.label3);
                this.Controls.Add(this.label2);
                this.Controls.Add(this.label1);
                this.Controls.Add(this.btnOK);
                this.Controls.Add(this.pictureBox);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
                this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
                this.MaximizeBox = false;
                this.Name = "frmAbout";
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                this.Text = "关于";
                this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmAbout_FormClosed);
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
                this.ResumeLayout(false);
                this.PerformLayout();

            }

            private void btnOK_Click(object sender, EventArgs e)
            {
                Close();
            }

            private void labCompany_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                ComFunction.OpenLink(CompanyLink);
            }

            private void labEmail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                System.Diagnostics.Process.Start("mailto: " + EmailLink);
            }

            private void pictureBox_Click(object sender, EventArgs e)
            {
                ComFunction.OpenLink(CompanyLink);
            }

            private void frmAbout_FormClosed(object sender, FormClosedEventArgs e)
            {
                IsShow = false;
            }
        }
    }
}