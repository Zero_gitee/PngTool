﻿namespace ZmrSoft
{
    namespace WinForm
    {
        partial class frmAbout
        {
            /// <summary>
            /// 必需的设计器变量。
            /// </summary>
            //private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// 清理所有正在使用的资源。
            /// </summary>
            /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows 窗体设计器生成的代码

            /// <summary>
            /// 设计器支持所需的方法 - 不要
            /// 使用代码编辑器修改此方法的内容。
            /// </summary>
            //private void InitializeComponent()
            //{
            //    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
            //    this.pictureBox = new System.Windows.Forms.PictureBox();
            //    this.label1 = new System.Windows.Forms.Label();
            //    this.label2 = new System.Windows.Forms.Label();
            //    this.label3 = new System.Windows.Forms.Label();
            //    this.label4 = new System.Windows.Forms.Label();
            //    this.label5 = new System.Windows.Forms.Label();
            //    this.textBox1 = new System.Windows.Forms.TextBox();
            //    this.button1 = new System.Windows.Forms.Button();
            //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            //    this.SuspendLayout();
            //    // 
            //    // pictureBox
            //    // 
            //    this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            //    this.pictureBox.BackColor = System.Drawing.Color.Transparent;
            //    this.pictureBox.Location = new System.Drawing.Point(286, 12);
            //    this.pictureBox.Name = "pictureBox";
            //    this.pictureBox.Size = new System.Drawing.Size(96, 96);
            //    this.pictureBox.TabIndex = 0;
            //    this.pictureBox.TabStop = false;
            //    // 
            //    // label1
            //    // 
            //    this.label1.AutoSize = true;
            //    this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //    this.label1.Location = new System.Drawing.Point(12, 9);
            //    this.label1.Name = "label1";
            //    this.label1.Size = new System.Drawing.Size(57, 12);
            //    this.label1.TabIndex = 1;
            //    this.label1.Text = "程序名称";
            //    // 
            //    // label2
            //    // 
            //    this.label2.AutoSize = true;
            //    this.label2.Location = new System.Drawing.Point(75, 9);
            //    this.label2.Name = "label2";
            //    this.label2.Size = new System.Drawing.Size(41, 12);
            //    this.label2.TabIndex = 2;
            //    this.label2.Text = "label2";
            //    // 
            //    // label3
            //    // 
            //    this.label3.AutoSize = true;
            //    this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //    this.label3.Location = new System.Drawing.Point(12, 38);
            //    this.label3.Name = "label3";
            //    this.label3.Size = new System.Drawing.Size(57, 12);
            //    this.label3.TabIndex = 3;
            //    this.label3.Text = "技术支持";
            //    // 
            //    // label4
            //    // 
            //    this.label4.AutoSize = true;
            //    this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //    this.label4.Location = new System.Drawing.Point(12, 67);
            //    this.label4.Name = "label4";
            //    this.label4.Size = new System.Drawing.Size(47, 12);
            //    this.label4.TabIndex = 4;
            //    this.label4.Text = "E-mail";
            //    // 
            //    // label5
            //    // 
            //    this.label5.AutoSize = true;
            //    this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //    this.label5.Location = new System.Drawing.Point(12, 96);
            //    this.label5.Name = "label5";
            //    this.label5.Size = new System.Drawing.Size(57, 12);
            //    this.label5.TabIndex = 5;
            //    this.label5.Text = "版权声明";
            //    // 
            //    // textBox1
            //    // 
            //    this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            //    this.textBox1.Location = new System.Drawing.Point(12, 114);
            //    this.textBox1.Multiline = true;
            //    this.textBox1.Name = "textBox1";
            //    this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            //    this.textBox1.Size = new System.Drawing.Size(370, 116);
            //    this.textBox1.TabIndex = 6;
            //    // 
            //    // button1
            //    // 
            //    this.button1.Location = new System.Drawing.Point(307, 236);
            //    this.button1.Name = "button1";
            //    this.button1.Size = new System.Drawing.Size(75, 23);
            //    this.button1.TabIndex = 7;
            //    this.button1.Text = "关闭";
            //    this.button1.UseVisualStyleBackColor = true;
            //    // 
            //    // frmAbout
            //    // 
            //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //    this.ClientSize = new System.Drawing.Size(394, 270);
            //    this.Controls.Add(this.button1);
            //    this.Controls.Add(this.textBox1);
            //    this.Controls.Add(this.label5);
            //    this.Controls.Add(this.label4);
            //    this.Controls.Add(this.label3);
            //    this.Controls.Add(this.label2);
            //    this.Controls.Add(this.label1);
            //    this.Controls.Add(this.pictureBox);
            //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            //    this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            //    this.MaximizeBox = false;
            //    this.Name = "frmAbout";
            //    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            //    this.Text = "关于";
            //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            //    this.ResumeLayout(false);
            //    this.PerformLayout();

            //}

            #endregion

            //private System.Windows.Forms.PictureBox pictureBox;
            //private System.Windows.Forms.Label label1;
            //private System.Windows.Forms.Label label2;
            //private System.Windows.Forms.Label label3;
            //private System.Windows.Forms.Label label4;
            //private System.Windows.Forms.Label label5;
            //private System.Windows.Forms.TextBox textBox1;
            //private System.Windows.Forms.Button button1;
        }
    }
}