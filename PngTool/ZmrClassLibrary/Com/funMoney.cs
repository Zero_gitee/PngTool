using System;
using System.Collections.Generic;
using System.Text;

namespace ZmrSoft
{
    public class funMoney
    {
        //字符常量
        static private string[] NO = new string[] { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };
        static private string[] DW = new string[] { "万", "仟", "佰", "拾", "", "仟", "佰", "拾", "", "仟", "佰", "拾", "", "", "角", "分" };

        /// <summary>
        /// 数字转中文
        /// </summary>
        /// <param name="s">格式为“0.00”的实数</param>
        /// <returns>转换后的汉字大写金额</returns>
        static public string NoToCn(string s)
        {
            string Ret = "";

            //不对‘0’或空字符串操作
            if ((s == "") || (s == "0"))
            {
                return "";
            }

            s = FormatNo(s);

            if (s == "")
            {
                return "";
            }

            string ls = "";
            for (int i = 0; i < 16; i++)
            {
                string n = s.Substring(i, 1);

                if (n == ".")
                {
                    ls = "";
                }
                else if (n == "0")
                {
                    int intN;
                    if (!int.TryParse(n, out intN))
                    {
                        intN = 0;
                    }
                    ls = NO[intN];
                }
                else
                {
                    int intN;
                    if (!int.TryParse(n, out intN))
                    {
                        intN = 0;
                    }
                    string d = DW[i];
                    ls = NO[intN] + d;
                }

                if (ls != "零")
                {
                    switch (i)
                    {
                        case 4:
                            {
                                ls += "亿";
                                break;
                            }
                        case 8:
                            {
                                ls += "万";
                                break;
                            }
                        case 12:
                            {
                                ls += "圆";
                                break;
                            }
                    }
                }
                else
                {
                    switch (i)
                    {
                        case 4:
                            {
                                ls += "亿零";
                                break;
                            }
                        case 8:
                            {
                                ls += "万零";
                                break;
                            }
                        case 12:
                            {
                                ls += "圆零";
                                break;
                            }
                    }
                }

                Ret += ls;
            }

            Ret = DelZero(Ret);

            //若最后是“圆”则加上“整”
            string Last = Ret.Substring(Ret.Length - 1);
            if (Last == "圆")
            {
                Ret += "整";
            }

            return Ret;
        }

        /// <summary>
        /// 采用四舍五入法格式化指定的实数
        /// </summary>
        /// <param name="s">合法的实数字符串</param>
        /// <param name="f">要保留的小数位数</param>
        /// <returns>格式化后的实数字符串</returns>
        static public string RealF(string s, byte f)
        {
            string Ret = s;

            double dRet;
            if (double.TryParse(s, out dRet))
            {

            }
            else
            {
                dRet = 0;

            }
            Ret = dRet.ToString("F" + f.ToString());

            return Ret;
        }

        #region 私有方法

        //子函数：把输入的数字金额转换城指定的格式
        static private string FormatNo(string No)
        {
            string Ret = "";

            //直接调用保留指定小数位数的函数
            Ret = RealF(No, 2);

            if (Ret == "")
            {
                return "";
            }

            //目前只支持16位以下的运算
            if (Ret.Length > 16)
            {
                return "";
            }

            //小数点以前的部分，如果位数不够，就加'0'
            while (Ret.Length < 16)
            {
                Ret = "0" + Ret;
            }

            return Ret;
        }

        //子函数：去掉多余的“零”
        static private string DelZero(string CH)
        {
            string Ret = CH;

            //去掉末尾的“零”
            while (Ret.Substring(Ret.Length - 1, 1) == "零")
            {
                Ret = Ret.Remove(Ret.Length - 1, 1);
            }

            //去掉前面的“零”和“亿”、“万”、“元”等单位
            while ((Ret.Substring(0, 1) == "零") || (Ret.Substring(0, 1) == "亿") || (Ret.Substring(0, 1) == "万") || (Ret.Substring(0, 1) == "元"))
            {
                Ret = Ret.Remove(0, 1);
            }

            //去掉中间多余的“零”
            while (Ret.IndexOf("零零") > -1)
            {
                Ret = Ret.Replace("零零", "零");
            }

            //去掉“零亿”
            while (Ret.IndexOf("零亿") > -1)
            {
                Ret = Ret.Remove(Ret.IndexOf("零亿"), 2);
            }

            //去掉“零万”
            while (Ret.IndexOf("零万") > -1)
            {
                Ret = Ret.Remove(Ret.IndexOf("零万"), 2);
            }

            //去掉“零圆”
            while (Ret.IndexOf("零圆") > -1)
            {
                Ret = Ret.Remove(Ret.IndexOf("零圆"), 2);
            }

            //去掉“零整”
            while (Ret.IndexOf("零整") > -1)
            {
                Ret = Ret.Remove(Ret.IndexOf("零整"), 2);
            }

            return Ret;
        }

        #endregion
    }
}
