using System;
using System.Collections.Generic;
using System.Text;

namespace ZmrSoft
{
    public class clsEncryption
    {
        /// <summary>
        /// ���ַ������м���
        /// </summary>
        /// <param name="Sou">Ҫ���ܵ��ַ���</param>
        /// <param name="Key">��Կ</param>
        public static string Encryption(string Sou, string Key)
        {
            byte intKey = 0;
            byte[] Byte = Encoding.Default.GetBytes(Key);
            for (int i = 0; i < Byte.Length; i++)
            {
                intKey += Byte[i];
            }

            return Encryption(Sou, intKey);
        }

        /// <summary>
        /// ���ַ������м���
        /// </summary>
        /// <param name="Sou">Ҫ���ܵ��ַ���</param>
        /// <param name="Key">��Կ</param>
        public static string Encryption(string Sou, byte Key)
        {
            byte[] Byte = Encoding.Default.GetBytes(Sou);
            Array.Reverse(Byte);
            for (int i = 0; i < Byte.Length; i++)
            {
                Byte[i] += Key;
            }

            return Encoding.Default.GetString(Byte);
        }

        /// <summary>
        /// ���ַ������н���
        /// </summary>
        /// <param name="Sou">Ҫ���ܵ��ַ���</param>
        /// <param name="Key">��Կ</param>
        public static string Decryption(string Sou, string Key)
        {
            byte intKey = 0;
            byte[] Byte = Encoding.Default.GetBytes(Key);
            for (int i = 0; i < Byte.Length; i++)
            {
                intKey += Byte[i];
            }

            return Decryption(Sou, intKey);
        }

        /// <summary>
        /// ���ַ������н���
        /// </summary>
        /// <param name="Sou">Ҫ���ܵ��ַ���</param>
        /// <param name="Key">��Կ</param>
        public static string Decryption(string Sou, byte Key)
        {
            byte[] Byte = Encoding.Default.GetBytes(Sou);
            for (int i = 0; i < Byte.Length; i++)
            {
                Byte[i] -= Key;
            }
            Array.Reverse(Byte);

            return Encoding.Default.GetString(Byte);
        }
    }
}
