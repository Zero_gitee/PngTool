namespace ZmrSoft
{
    namespace WinForm
    {
        partial class AlphaBtn
        {
            /// <summary>
            /// 必需的设计器变量。
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// 清理所有正在使用的资源。
            /// </summary>
            /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows 窗体设计器生成的代码

            /// <summary>
            /// 设计器支持所需的方法 - 不要
            /// 使用代码编辑器修改此方法的内容。
            /// </summary>
            private void InitializeComponent()
            {
                this.SuspendLayout();
                // 
                // AlphaBtn
                // 
                this.ClientSize = new System.Drawing.Size(292, 268);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.Name = "AlphaBtn";
                this.ShowIcon = false;
                this.ShowInTaskbar = false;
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                this.MouseEnter += new System.EventHandler(this.AlphaBtn_MouseEnter);
                this.MouseLeave += new System.EventHandler(this.AlphaBtn_MouseLeave);
                this.Load += new System.EventHandler(this.AlphaBtn_Load);
                this.ResumeLayout(false);

            }

            #endregion
        }
    }
}
