using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZmrSoft.WinForm;

namespace ZmrSoft
{
    namespace WinForm
    {
        //public partial class AlphaBtn : Form
        public partial class AlphaBtn : AlphaForm
        {
            private Bitmap _BackImage = null;
            private int _BtnSize = 32;
            /// <summary>
            /// ���ָ��ʱ�ı���ͼƬ
            /// </summary>
            private Bitmap _MouseEnterImage = null;
            /// <summary>
            /// ����뿪��ı���ͼƬ
            /// </summary>
            private Bitmap _MouseLeaveImage = null;

            public AlphaBtn()
            {
                InitializeComponent();

                BtnSize = 32;
            }

            public AlphaBtn(Bitmap BackImg)
            {
                InitializeComponent();

                BtnSize = 32;
                BackImage = BackImg;
            }

            public AlphaBtn(Bitmap BackImg, int Size)
            {
                InitializeComponent();

                BtnSize = Size;
                BackImage = BackImg;
            }

            /// <summary>
            /// �ߴ�
            /// </summary>
            public int BtnSize
            {
                get
                {
                    return _BtnSize;
                }
                set
                {
                    _BtnSize = value;
                    Size = new Size(value, value);
                }
            }

            /// <summary>
            /// ����ͼƬ
            /// </summary>
            public Bitmap BackImage
            {
                get
                {
                    return _BackImage;
                }
                set
                {
                    _BackImage = value;
                    UpdateBackImage();
                }
            }

            /// <summary>
            /// ���±���ͼƬ
            /// </summary>
            private void UpdateBackImage()
            {
                if (BackImage != null)
                {
                    _MouseEnterImage = DrawImageByXY(1, 0);
                    _MouseLeaveImage = DrawImageByXY(0, 1);
                }
            }

            /// <summary>
            /// ����ԭ��ƫ��ֵ������
            /// </summary>
            /// <param name="x">x��ƫ����</param>
            /// <param name="y">y��ƫ����</param>
            private Bitmap DrawImageByXY(int x, int y)
            {
                int H = BtnSize - (x + y);
                int W = BtnSize - (x + y);
                Bitmap Ret = new Bitmap(BtnSize, BtnSize);
                using (Graphics g = Graphics.FromImage(Ret))
                {
                    g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                    g.DrawImage(BackImage, x, y, W, H);
                }

                return Ret;
            }

            private void AlphaBtn_MouseEnter(object sender, EventArgs e)
            {
                Activate();
                BackgroundImage = _MouseEnterImage;
                SetBits();
            }

            private void AlphaBtn_MouseLeave(object sender, EventArgs e)
            {
                BackgroundImage = _MouseLeaveImage;
                SetBits();
            }

            private void AlphaBtn_Load(object sender, EventArgs e)
            {
                BackgroundImage = _MouseLeaveImage;
                SetBits();
            }
        }
    }
}