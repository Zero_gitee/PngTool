//#####################################################################################
//★★  根据华普软件提供源码改编  2008.8.30 zmr★★
//#####################################################################################

using System;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Collections;

namespace ZmrSoft
{
    namespace WinForm
    {
        public class AlphaForm : Form
        {
            #region WindowsAPI

            //实现鼠标移动
            [DllImport("user32.dll")]
            public static extern bool ReleaseCapture();
            [DllImport("user32.dll")]
            public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

            //实现锁定到桌面
            [DllImport("User32.dll", EntryPoint = "FindWindow")]
            private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
            [DllImport("User32.dll", EntryPoint = "SetParent")]
            private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
            [DllImport("User32.dll", EntryPoint = "GetDesktopWindow")]
            private static extern IntPtr GetDesktopWindow();

            //实现鼠标穿透
            [DllImport("user32", EntryPoint = "SetWindowLong")]
            private static extern uint SetWindowLong(IntPtr hwnd, int nIndex, uint dwNewLong);
            [DllImport("user32", EntryPoint = "GetWindowLong")]
            private static extern uint GetWindowLong(IntPtr hwnd, int nIndex);


            #endregion

            #region 变量

            private Point OldLocation = new Point(0, 0); //鼠标拖动相关变量，拖动前窗体位置
            private bool mouseDown = false; //鼠标拖动相关变量，判断左键是否被按下，以此决定是否在拖动
            private byte FAlphaValue = 255;       //透明度
            private bool FIsPenetrate = false;     //是否穿透
            private bool FIsInDesktop = false;     //是否嵌入桌面
            private bool FIsTopMost = false;       //是否总在最前
            private bool FPosFixed = false;       //是否固定位置
            private bool FSliding = false;       //是否使用滑动效果
            private bool FAlphaValueChange = false; //是否允许渐变
            private byte FBackAlphaValue = 0;  //背景透明度(默认完全透明，但有时候需要透明度为1以便操作)
            private bool FCanSize = false; //是否允许调整窗体大小

            //Windows消息常量
            public const int WM_SYSCOMMAND = 0x0112;
            public const int SC_MOVE = 0xF010;
            public const int HTCAPTION = 0x0002;

            //实现鼠标穿透
            private const uint WS_EX_LAYERED = 0x80000;
            private const int WS_EX_TRANSPARENT = 0x20;
            private const int GWL_STYLE = (-16);
            private const int GWL_EXSTYLE = (-20);
            private const int LWA_ALPHA = 0x2;

            //用于透明度渐变
            private byte OldAlphaValue;
            protected Timer ShowTimerUp;
            protected Timer ShowTimerDown;

            //用于滑动效果
            private Point OldPos = new Point(0, 0);
            private Timer BackPos;
            private bool IsMoving = false;

            //关闭后不再刷新
            private bool IsClosing = false;

            //调整窗体大小相关变量
            const int WM_NCHITTEST = 0x0084;
            const int HTLEFT = 10;
            const int HTRIGHT = 11;
            const int HTTOP = 12;
            const int HTTOPLEFT = 13;
            const int HTTOPRIGHT = 14;
            const int HTBOTTOM = 15;
            const int HTBOTTOMLEFT = 0x10;
            const int HTBOTTOMRIGHT = 17;

            #endregion

            #region 构造函数

            public AlphaForm()
            {
                FormBorderStyle = FormBorderStyle.None;

                //实现鼠标拖动的方法之一：等同于拖动标题栏，适用于多个窗体需要同步移动的情况
                //MouseDown += new MouseEventHandler(BaseAlphaForm_MouseDown); 

                //实现鼠标拖动的方法之二：实时拖动
                MouseDown += new MouseEventHandler(AlphaForm_MouseDown);
                MouseUp += new MouseEventHandler(AlphaForm_MouseUp);
                MouseMove += new MouseEventHandler(AlphaForm_MouseMove);

                //实现透明度渐变
                ShowTimerUp = new Timer();
                ShowTimerUp.Enabled = false;
                ShowTimerUp.Interval = 1;
                ShowTimerUp.Tick += new EventHandler(ShowTimerUp_Tick);
                ShowTimerDown = new Timer();
                ShowTimerDown.Enabled = false;
                ShowTimerDown.Interval = 1;
                ShowTimerDown.Tick += new EventHandler(ShowTimerDown_Tick);
                MouseEnter += new EventHandler(AlphaForm_MouseEnter);
                MouseLeave += new EventHandler(AlphaForm_MouseLeave);

                //实现滑动效果
                BackPos = new Timer();
                BackPos.Enabled = false;
                BackPos.Interval = 1;
                BackPos.Tick += new EventHandler(BackPos_Tick);

                //关闭后不再刷新
                FormClosing += new FormClosingEventHandler(AlphaForm_FormClosing);
            }

            void AlphaForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                IsClosing = true;
            }

            void BackPos_Tick(object sender, EventArgs e)
            {
                //步长
                int StepX = 1, StepY = 1;
                //确定方向
                if (Location.X > OldPos.X)
                {
                    StepX = 1;
                }
                else
                {
                    StepX = -1;
                }
                if (Location.Y > OldPos.Y)
                {
                    StepY = 1;
                }
                else
                {
                    StepY = -1;
                }
                //计算步长
                int X = Math.Abs(Location.X - OldPos.X);
                int Y = Math.Abs(Location.Y - OldPos.Y);
                if (X > Y)
                {
                    if (Y != 0)
                    {
                        StepX = StepX * Math.Abs(StepY) * (X / Y);
                    }
                }
                else
                {
                    if (X != 0)
                    {
                        StepY = StepY * Math.Abs(StepX) * (Y / X);
                    }
                }

                //根据步长移动
                if (Math.Abs(Location.X - OldPos.X) > Math.Abs(StepX))
                {
                    Location = new Point(Location.X - StepX, Location.Y);
                }
                else
                {
                    Location = new Point(OldPos.X, Location.Y);
                }

                if (Math.Abs(Location.Y - OldPos.Y) > Math.Abs(StepY))
                {
                    Location = new Point(Location.X, Location.Y - StepY);
                }
                else
                {
                    Location = new Point(Location.X, OldPos.Y);
                }
                //移动结束，还原
                if ((Location.X == OldPos.X) && (Location.Y == OldPos.Y))
                {
                    BackPos.Enabled = false;
                    IsMoving = false;
                }
            }

            void ShowTimerDown_Tick(object sender, EventArgs e)
            {
                if (AlphaValue > OldAlphaValue)
                {
                    AlphaValue = (byte)(AlphaValue - 1);
                }
                else
                {
                    ShowTimerDown.Enabled = false;
                }
            }

            void AlphaForm_MouseLeave(object sender, EventArgs e)
            {
                ShowTimerUp.Enabled = false;
                ShowTimerDown.Enabled = true;
            }

            void AlphaForm_MouseEnter(object sender, EventArgs e)
            {
                if ((Owner != null) && (Owner.ContainsFocus))
                {
                    Activate();
                }
                if (ShowTimerDown.Enabled == false)
                {
                    OldAlphaValue = AlphaValue;
                }
                ShowTimerUp.Enabled = AlphaValueChange;
            }

            void ShowTimerUp_Tick(object sender, EventArgs e)
            {
                if (AlphaValue < 250)
                {
                    AlphaValue = (byte)(AlphaValue + 5);
                }
            }

            void AlphaForm_MouseMove(object sender, MouseEventArgs e)
            {
                if (mouseDown)
                {
                    Left += (e.X - OldLocation.X);
                    Top += (e.Y - OldLocation.Y);
                }
            }

            void AlphaForm_MouseUp(object sender, MouseEventArgs e)
            {
                mouseDown = false;
                if (PosFixed && Sliding)
                {
                    BackPos.Enabled = true;
                    IsMoving = true;
                }
            }

            void AlphaForm_MouseDown(object sender, MouseEventArgs e)
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (!IsMoving)
                    {
                        OldPos = new Point(Location.X, Location.Y);
                    }

                    OldLocation = e.Location;
                    mouseDown = !FPosFixed || (FPosFixed && Sliding);
                }
            }

            #endregion

            #region 重载

            protected override void OnHandleCreated(EventArgs e)
            {
                InitializeStyles();
                base.OnHandleCreated(e);
            }

            protected override CreateParams CreateParams
            {
                get
                {
                    CreateParams cParms = base.CreateParams;
                    cParms.ExStyle |= 0x00080000; // WS_EX_LAYERED
                    return cParms;
                }
            }

            protected override void WndProc(ref Message m)
            {
                base.WndProc(ref m);

                //调整窗体大小
                switch (m.Msg)
                {
                    case WM_NCHITTEST:
                        if (CanSize)
                        {
                            Point vPoint = new Point((int)m.LParam & 0xFFFF,
                                (int)m.LParam >> 16 & 0xFFFF);
                            vPoint = PointToClient(vPoint);
                            if (vPoint.X <= 5)
                                if (vPoint.Y <= 5)
                                    m.Result = (IntPtr)HTTOPLEFT;
                                else if (vPoint.Y >= Height - 5)
                                    m.Result = (IntPtr)HTBOTTOMLEFT;
                                else m.Result = (IntPtr)HTLEFT;
                            else if (vPoint.X >= Width - 5)
                                if (vPoint.Y <= 5)
                                    m.Result = (IntPtr)HTTOPRIGHT;
                                else if (vPoint.Y >= Height - 5)
                                    m.Result = (IntPtr)HTBOTTOMRIGHT;
                                else m.Result = (IntPtr)HTRIGHT;
                            else if (vPoint.Y <= 5)
                                m.Result = (IntPtr)HTTOP;
                            else if (vPoint.Y >= Height - 5)
                                m.Result = (IntPtr)HTBOTTOM;
                        }

                        break;
                }
            }

            #endregion

            #region 属性

            /// <summary>
            /// 透明度，值在0~255之间
            /// </summary>        
            public virtual byte AlphaValue
            {
                get
                {
                    return FAlphaValue;
                }
                set
                {
                    FAlphaValue = value;
                    SetBits();
                }
            }

            /// <summary>
            /// 背景透明度，值在0~255之间
            /// </summary>        
            public virtual byte BackAlphaValue
            {
                get
                {
                    return FBackAlphaValue;
                }
                set
                {
                    FBackAlphaValue = value;
                    SetBits();
                }
            }

            /// <summary>
            /// 鼠标穿透
            /// </summary>   
            public bool MousePenetrate
            {
                get
                {
                    return FIsPenetrate;
                }
                set
                {

                    if (value)
                    {
                        Penetrate(Handle);
                    }
                    else
                    {
                        FormBorderStyle = FormBorderStyle.None;
                    }
                    FIsPenetrate = value;
                }
            }

            /// <summary>
            /// 嵌入桌面
            /// </summary>   
            public bool InDesktop
            {
                get
                {
                    return FIsInDesktop;
                }
                set
                {

                    if (value)
                    {
                        if (IsTopMost)
                        {
                            LockIntoDesktopTop();
                        }
                        else
                        {
                            LockIntoDesktopBottom();
                        }
                    }
                    else
                    {
                        //权宜之计
                        IsTopMost = false;
                        LockIntoDesktopTop();
                    }
                    FIsInDesktop = value;
                }
            }

            /// <summary>
            /// 总在最前
            /// </summary>   
            public bool IsTopMost
            {
                get
                {
                    return FIsTopMost;
                }
                set
                {
                    if (value)
                    {
                        TopMost = true;
                        FIsTopMost = value;
                        //一定要和“嵌入桌面”同时存在
                        InDesktop = true;
                    }
                    else
                    {
                        TopMost = false;
                        FIsTopMost = value;
                        if (InDesktop == true)
                        {
                            InDesktop = true;
                        }
                    }
                }
            }

            /// <summary>
            /// 固定位置
            /// </summary>   
            public bool PosFixed
            {
                get
                {
                    return FPosFixed;
                }
                set
                {
                    FPosFixed = value;
                }
            }

            /// <summary>
            /// 滑动效果
            /// </summary>   
            public bool Sliding
            {
                get
                {
                    return FSliding;
                }
                set
                {
                    FSliding = value;
                }
            }

            /// <summary>
            /// 是否允许渐变
            /// </summary>   
            public bool AlphaValueChange
            {
                get
                {
                    return FAlphaValueChange;
                }
                set
                {
                    FAlphaValueChange = value;
                }
            }

            /// <summary>
            /// 是否允许调整大小
            /// </summary>   
            public bool CanSize
            {
                get
                {
                    return FCanSize;
                }
                set
                {
                    FCanSize = value;
                }
            }

            #endregion

            #region 方法

            void BaseAlphaForm_MouseDown(object sender, MouseEventArgs e)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);
            }

            /// <summary>
            /// 设置背景图片
            /// </summary>
            virtual public void SetBits()
            {
                if ((BackgroundImage != null) && (!IsClosing))
                {
                    using (Bitmap bitmap = new Bitmap(Width, Height))
                    {
                        //根据透明度画背景
                        using (Graphics g = Graphics.FromImage(bitmap))
                        {
                            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                            //g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                            g.Clear(Color.FromArgb(BackAlphaValue, 0, 0, 0));

                            g.DrawImage(BackgroundImage, 0, 0);
                        }

                        if (!Bitmap.IsCanonicalPixelFormat(bitmap.PixelFormat) || !Bitmap.IsAlphaPixelFormat(bitmap.PixelFormat))
                            throw new ApplicationException("图片必须是32位带Alhpa通道的图片。");

                        IntPtr oldBits = IntPtr.Zero;
                        IntPtr screenDC = Win32.GetDC(IntPtr.Zero);
                        IntPtr hBitmap = IntPtr.Zero;
                        IntPtr memDc = Win32.CreateCompatibleDC(screenDC);

                        try
                        {
                            Win32.Point topLoc = new Win32.Point(Left, Top);
                            Win32.Size bitMapSize = new Win32.Size(Width, Height);
                            Win32.BLENDFUNCTION blendFunc = new Win32.BLENDFUNCTION();
                            Win32.Point srcLoc = new Win32.Point(0, 0);

                            hBitmap = bitmap.GetHbitmap(Color.FromArgb(0));
                            oldBits = Win32.SelectObject(memDc, hBitmap);

                            blendFunc.BlendOp = Win32.AC_SRC_OVER;
                            blendFunc.SourceConstantAlpha = FAlphaValue;
                            blendFunc.AlphaFormat = Win32.AC_SRC_ALPHA;
                            blendFunc.BlendFlags = 0;

                            Win32.UpdateLayeredWindow(Handle, screenDC, ref topLoc, ref bitMapSize, memDc, ref srcLoc, 0, ref blendFunc, Win32.ULW_ALPHA);
                        }
                        finally
                        {
                            if (hBitmap != IntPtr.Zero)
                            {
                                Win32.SelectObject(memDc, oldBits);
                                Win32.DeleteObject(hBitmap);
                            }
                            Win32.ReleaseDC(IntPtr.Zero, screenDC);
                            Win32.DeleteDC(memDc);
                        }
                    }
                }
            }

            private void InitializeStyles()
            {
                SetStyle(ControlStyles.AllPaintingInWmPaint, true);
                SetStyle(ControlStyles.UserPaint, true);
                UpdateStyles();
            }

            /// <summary>
            /// 嵌入桌面顶层
            /// </summary>
            protected void LockIntoDesktopTop()
            {
                IntPtr hDeskTop = GetDesktopWindow();
                SetParent(Handle, hDeskTop);
                TopMost = IsTopMost;
            }

            /// <summary>
            /// 嵌入桌面底层
            /// </summary>
            protected void LockIntoDesktopBottom()
            {
                IntPtr hDeskTop = FindWindow("Progman", "Program Manager");     //真正在桌面上
                SetParent(Handle, hDeskTop);
            }

            /// <summary>
            /// 使窗口有鼠标穿透功能
            /// </summary>
            protected void Penetrate(IntPtr hdl)
            {
                uint intExTemp = GetWindowLong(hdl, GWL_EXSTYLE);
                uint oldGWLEx = SetWindowLong(hdl, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED);
            }

            #endregion

        }
    }
}

/// <summary>
/// Wind32API声明
/// </summary>
internal class Win32
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Size
    {
        public Int32 cx;
        public Int32 cy;

        public Size(Int32 x, Int32 y)
        {
            cx = x;
            cy = y;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BLENDFUNCTION
    {
        public byte BlendOp;
        public byte BlendFlags;
        public byte SourceConstantAlpha;
        public byte AlphaFormat;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public Int32 x;
        public Int32 y;

        public Point(Int32 x, Int32 y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public const byte AC_SRC_OVER = 0;
    public const Int32 ULW_ALPHA = 2;
    public const byte AC_SRC_ALPHA = 1;

    [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

    [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr GetDC(IntPtr hWnd);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObj);

    [DllImport("user32.dll", ExactSpelling = true)]
    public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

    [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern int DeleteDC(IntPtr hDC);

    [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern int DeleteObject(IntPtr hObj);

    [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern int UpdateLayeredWindow(IntPtr hwnd, IntPtr hdcDst, ref Point pptDst, ref Size psize, IntPtr hdcSrc, ref Point pptSrc, Int32 crKey, ref BLENDFUNCTION pblend, Int32 dwFlags);

    [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr ExtCreateRegion(IntPtr lpXform, uint nCount, IntPtr rgnData);
}

internal struct LineSeg
{
    public int x1;
    public int x2;
    public int y;
    public int dy;

    public LineSeg(int x1, int x2, int y, int dy)
    {
        this.x1 = x1;
        this.x2 = x2;
        this.y = y;
        this.dy = dy;
    }
}