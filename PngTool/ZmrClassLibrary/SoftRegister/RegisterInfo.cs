using System;
using System.Collections.Generic;
using System.Text;

namespace ZmrSoft
{
    /// <summary>
    /// 注册信息
    /// </summary>
    [Serializable()]
    public class RegisterInfo : Data.Data
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        private string _UserName = "";
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        /// <summary>
        /// 版本
        /// </summary>
        private int _Version = 0;
        /// <summary>
        /// 版本
        /// </summary>
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        /// <summary>
        /// 机器码
        /// </summary>
        private string _MachineCode = "";
        /// <summary>
        /// 机器码
        /// </summary>
        public string MachineCode
        {
            get { return _MachineCode; }
            set { _MachineCode = value; }
        }

        /// <summary>
        /// 注册码
        /// </summary>
        private string _License = "";
        /// <summary>
        /// 注册码
        /// </summary>
        public string License
        {
            get { return _License; }
            set { _License = value; }
        }

        /// <summary>
        /// 注册时间
        /// </summary>
        private DateTime _RegDateTime = DateTime.MinValue;
        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegDateTime
        {
            get { return _RegDateTime; }
            set { _RegDateTime = value; }
        }

        /// <summary>
        /// 注册月数
        /// </summary>
        private int _RegMonth = 0;
        /// <summary>
        /// 注册月数
        /// </summary>
        public int RegMonth
        {
            get { return _RegMonth; }
            set { _RegMonth = value; }
        }
    }
}
