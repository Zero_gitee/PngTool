using System;
using System.Collections.Generic;
using System.Text;
using System.Management;

namespace ZmrSoft
{
    /// <summary>
    /// 软件注册
    /// </summary>
    public class SoftRegister
    {
        /// <summary>
        /// 获取机器码
        /// </summary>
        public static string GetMachineCode()
        {
            return GetCPUID();
        }

        /// <summary>
        /// 生成注册码
        /// </summary>
        public static string BuildLicense(string MachineCode)
        {
            return clsEncryption.Encryption(MachineCode, "lyc");
        }

        /// <summary>
        /// 验证是否注册
        /// </summary>
        public static bool CheckReg(RegisterInfo RegInfo)
        {
            return Check(RegInfo);
        }

        //获取CPU信息
        static private string GetCPUID()
        {
            ManagementClass mc = new ManagementClass("Win32_Processor");     //
            ManagementObjectCollection moc = mc.GetInstances();
            String strCpuID = null;
            foreach (ManagementObject mo in moc)
            {
                strCpuID = mo.Properties["ProcessorId"].Value.ToString();
                break;
            }
            return strCpuID;
        }

        //验证是否注册
        static private bool Check(RegisterInfo RegInfo)
        {
            string MachineCode = GetMachineCode();
            string License = BuildLicense(MachineCode);

            ////注册时间
            //bool RetDate = false;
            //if (RegInfo.RegDateTime == DateTime.MinValue) //永久
            //{
            //    RetDate = true;
            //}
            //else
            //{
            //    RetDate = ((RegInfo.RegDateTime < DateTime.Now) && (RegInfo.RegDateTime.AddMonths(RegInfo.RegMonth) > DateTime.Now));
            //}

            //return ((MachineCode == RegInfo.MachineCode) && (License == RegInfo.License) && RetDate);

            return ((MachineCode == RegInfo.MachineCode) && (License == RegInfo.License));
        }
    }
}
