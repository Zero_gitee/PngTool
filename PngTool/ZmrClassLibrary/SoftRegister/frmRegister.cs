using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZmrSoft
{
    public partial class frmRegister : Form
    {
        //ע��
        static public RegisterInfo Register()
        {
            RegisterInfo Ret = null;

            using (frmRegister frm = new frmRegister())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Ret = new RegisterInfo();
                    Ret.UserName = frm.edtUserName.Text;
                    Ret.MachineCode = frm.edtMachineCode.Text;
                    Ret.License = frm.edtLicense.Text;
                }
            }

            return Ret;
        }

        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            edtMachineCode.Text = SoftRegister.GetMachineCode();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void brnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}