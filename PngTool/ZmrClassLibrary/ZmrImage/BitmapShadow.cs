using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace ZmrSoft.ZmrImage
{
    public class BitmapShadow
    {
        /// <summary>
        /// 阴影半径
        /// </summary>
        private int radius = 3;

        /// <summary>
        ///  阴影距离
        /// </summary>
        private int distance = 3;    
  
        /// <summary>
        ///  阴影输出角度(左边平行处为0度。顺时针方向)
        /// </summary>
        private double angle = 45;

        /// <summary>
        /// 阴影文字的不透明度
        /// </summary>
        private byte alpha = 255;

        /// <summary>
        /// 高斯卷积矩阵
        /// </summary>
        private int[] gaussMatrix;
        /// <summary>
        /// 卷积核
        /// </summary>
        private int nuclear = 0;

        /// <summary>
        /// 阴影半径
        /// </summary>
        public int Radius
        {
            get
            {
                return radius;
            }
            set
            {
                if (radius != value)
                {
                    radius = value;
                    MakeGaussMatrix();
                }
            }
        }

        /// <summary>
        ///  阴影距离
        /// </summary>
        public int Distance
        {
            get
            {
                return distance;
            }
            set
            {
                distance = value;
            }
        }

        /// <summary>
        ///  阴影输出角度(左边平行处为0度。顺时针方向)
        /// </summary>
        public double Angle
        {
            get
            {
                return angle;
            }
            set
            {
                angle = value;
            }
        }

        /// <summary>
        /// 阴影文字的不透明度
        /// </summary>
        public byte Alpha
        {
            get
            {
                return alpha;
            }
            set
            {
                alpha = value;
            }
        }

        /// <summary>
        /// 对文字阴影位图按阴影半径计算的高斯矩阵进行卷积模糊
        /// </summary>
        /// <param name="bmp">文字阴影位图</param>
        private unsafe void MaskShadow(Bitmap bmp)
        {
            if (nuclear == 0)
                MakeGaussMatrix();
            Rectangle r = new Rectangle(0, 0, bmp.Width, bmp.Height);
            // 克隆临时位图，作为卷积源
            Bitmap tmp = (Bitmap)bmp.Clone();
            BitmapData dest = bmp.LockBits(r, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            BitmapData source = tmp.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            try
            {
                // 源首地址(0, 0)的Alpha字节，也就是目标首像素的第一个卷积乘数的像素点
                byte* ps = (byte*)source.Scan0;
                ps += 3;
                // 目标地址为卷积半径点(radius, radius)的Alpha字节
                byte* pd = (byte*)dest.Scan0;
                pd += (radius * (dest.Stride + 4) + 3);
                // 位图实际卷积的部分
                int width = dest.Width - radius * 2;
                int height = dest.Height - radius * 2;
                int matrixSize = radius * 2 + 1;
                // 卷积矩阵字节偏移
                int mOffset = dest.Stride - matrixSize * 4;
                // 行尾卷积半径(radius)的偏移
                int rOffset = radius * 8;
                int count = matrixSize * matrixSize;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {

                        byte* s = ps - mOffset;
                        int v = 0;
                        for (int i = 0; i < count; i++, s += 4)
                        {
                            if ((i % matrixSize) == 0)
                                s += mOffset;           // 卷积矩阵的换行
                            v += gaussMatrix[i] * *s;   // 位图像素点Alpha的卷积值求和
                        }
                        // 目标位图被卷积像素点Alpha等于卷积和除以卷积核
                        *pd = (byte)(v / nuclear);
                        pd += 4;
                        ps += 4;
                    }
                    pd += rOffset;
                    ps += rOffset;
                }
            }
            finally
            {
                tmp.UnlockBits(source);
                bmp.UnlockBits(dest);
                tmp.Dispose();
            }
        }

        /// <summary>
        /// 按给定的阴影半径生成高斯卷积矩阵
        /// </summary>
        private void MakeGaussMatrix()
        {
            double Q = (double)radius / 2.0;
            if (Q == 0.0)
                Q = 0.1;
            int n = radius * 2 + 1;
            int index = 0;
            nuclear = 0;
            gaussMatrix = new int[n * n];

            for (int x = -radius; x <= radius; x++)
            {
                for (int y = -radius; y <= radius; y++)
                {
                    gaussMatrix[index] = (int)Math.Round(Math.Exp(-((double)x * x + y * y) / (2.0 * Q * Q)) /
                                                         (2.0 * Math.PI * Q * Q) * 1000.0);
                    nuclear += gaussMatrix[index];
                    index++;
                }
            }
        }

        /// <summary>
        /// 为输入位图添加阴影
        /// </summary>
        /// <param name="souBitmap">原始图像</param>
        public Bitmap AddBitmapShadow(Bitmap souBitmap)
        {
            //位移量
            //int x = radius + distance;
            //新尺寸
            Size NewSize = new Size(souBitmap.Width + radius * 2 + distance, souBitmap.Height + radius * 2 + distance);

            Bitmap Ret = new Bitmap(NewSize.Width, NewSize.Height, PixelFormat.Format32bppArgb);

            Graphics g = Graphics.FromImage(Ret);
            try
            {
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

                //得到阴影
                Bitmap GrayBitmap = GetBitmapShadow(souBitmap);

                //按阴影角度、半径和距离输出文字阴影到给定的画布
                RectangleF dr = new RectangleF(0, 0, GrayBitmap.Width, GrayBitmap.Height);
                RectangleF sr = new RectangleF(0, 0, GrayBitmap.Width, GrayBitmap.Height);
                dr.Offset((float)(Math.Cos(Math.PI * angle / 180.0) * distance),
                          (float)(Math.Sin(Math.PI * angle / 180.0) * distance));

                //画阴影
                g.DrawImage(GrayBitmap, dr, sr, GraphicsUnit.Pixel);
                g.DrawImage(GrayBitmap, dr, sr, GraphicsUnit.Pixel); //画两次加深

                //画原始图像
                g.DrawImage(souBitmap, radius, radius, souBitmap.Width, souBitmap.Height);
            }
            catch
            {

            }
            finally
            {
                g.Dispose();
            }

            return Ret;
        }

        /// <summary>
        /// 根据输入位图得到它的阴影
        /// </summary>
        /// <param name="souBitmap">原始图像</param>
        private Bitmap GetBitmapShadow(Bitmap souBitmap)
        {
            //得到去色后的图片
            Bitmap GrayBitmap = ZmrImage.ChangeColor(souBitmap, Color.Black);
            //根据新尺寸调整
            GrayBitmap = GetNewImage(GrayBitmap);
            // 制造阴影模糊
            MaskShadow(GrayBitmap);

            Bitmap Ret = new Bitmap(GrayBitmap.Width + distance, GrayBitmap.Height + distance);
            Graphics g = Graphics.FromImage(Ret);
            try
            {
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

                RectangleF sr = new RectangleF(0, 0, GrayBitmap.Width, GrayBitmap.Height);
                g.DrawImage(GrayBitmap, sr);
            }
            catch
            {

            }
            finally
            {
                g.Dispose();
            }

            return Ret;
        }

        ////根据半径和距离计算扩展后的尺寸
        //private Size GetNewSize(Bitmap souBitmap)
        //{
        //    return new Size(souBitmap.Width + radius + distance, souBitmap.Height + radius + distance);
        //}

        //得到扩展图像
        private Bitmap GetNewImage(Bitmap souBitmap)
        {
            Size NewSize = new Size(souBitmap.Width + radius * 2, souBitmap.Height + radius * 2);

            Bitmap Ret = new Bitmap(NewSize.Width, NewSize.Height);
            Graphics g = Graphics.FromImage(Ret);
            try
            {
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

                g.DrawImage(souBitmap, radius, radius, souBitmap.Width, souBitmap.Height);
            }
            catch
            {

            }
            finally
            {
                g.Dispose();
            }

            return Ret;
        }
    }
}
