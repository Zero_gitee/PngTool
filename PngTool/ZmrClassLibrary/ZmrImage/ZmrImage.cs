using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace ZmrSoft.ZmrImage
{
    public class ZmrImage
    {
        //通过图片文件取Bitmap图片
        static public Bitmap GetImageFromFile(string FileName)
        {
            Bitmap Ret = null;
            //Icon格式特殊处理
            if (Path.GetExtension(FileName).ToLower() == ".ico")
            {

                Bitmap Img = new Bitmap(48, 48);
                using (Icon ico = new Icon(FileName))
                {
                    using (Graphics g = Graphics.FromImage(Img))
                    {
                        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                        g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        g.DrawIcon(ico, new Rectangle(0, 0, 48, 48));
                    }
                }
                Ret = Img;
            }
            else
            {
                Ret = new Bitmap(FileName);
            }
            return Ret;
        }

        //设置图片透明度
        static public Bitmap SetImageAlphaValue(Image souImage, float Opacity)
        {
            Bitmap Ret = new Bitmap(souImage.Width, souImage.Height);

            //方法一
            //float[][] nArray ={ new float[] { 1, 0, 0, 0, 0 }, new float[] { 0, 1, 0, 0, 0 }, new float[] { 0, 0, 1, 0, 0 }, new float[] { 0, 0, 0, Opacity, 0 }, new float[] { 0, 0, 0, 0, 1 } };
            //ColorMatrix matrix = new ColorMatrix(nArray);
            //ImageAttributes attributes = new ImageAttributes();
            //attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            //方法二
            ImageAttributes attributes = new ImageAttributes();
            ColorMatrix matrix = new ColorMatrix();
            matrix.Matrix33 = Opacity;
            attributes.SetColorMatrix(matrix); 

            using (Graphics g = Graphics.FromImage(Ret))
            {
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.DrawImage(souImage, new Rectangle(0, 0, souImage.Width, souImage.Height), 0, 0, souImage.Width, souImage.Height, GraphicsUnit.Pixel, attributes);
            }

            return Ret;
        }

        #region 参考代码
        //去色
        //static public Bitmap DelColor(Image souImage)
        //{
        //    Bitmap Ret = new Bitmap(souImage.Width, souImage.Height);

        //    using (Graphics g = Graphics.FromImage(Ret))
        //    {
        //        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
        //        g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        //        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //        g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
        //        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //        g.Clear(Color.White);

        //        Bitmap image = new Bitmap(souImage);
        //        int Width = image.Width - 1;
        //        int Height = image.Height - 1;

        //        //绘制原图   
        //        g.DrawImage(image, 0, 0);
        //        //g.TranslateTransform(image.Width, 0);

        //        /*image2、image3分别用来保存最大值法   
        //        和加权平均法处理的灰度图像*/
        //        Bitmap image2 = image.Clone(new Rectangle(0, 0, image.Width,
        //        image.Height), PixelFormat.DontCare);
        //        Bitmap image3 = image.Clone(new Rectangle(0, 0, image.Width,
        //        image.Height), PixelFormat.DontCare);

        //        Color color;
        //        //使用平均值进行灰度处理   
        //        for (int i = Width; i >= 0; i--)
        //            for (int j = Height; j >= 0; j--)
        //            {
        //                color = image.GetPixel(i, j);
        //                //求出平均三个色彩分量的平均值   
        //                int middle = (color.R +
        //                color.G + color.B) / 3;
        //                Color colorResult = Color.FromArgb(255, middle, middle, middle);
        //                image.SetPixel(i, j, colorResult);
        //            }
        //        //重新绘制灰度化图   
        //        g.DrawImage(
        //        image, new Rectangle(0, 0, Width, Height));

        //        //在新位置显示最大值法进行灰度处理的结果   
        //        //g.TranslateTransform(image.Width, 0);
        //        //使用最大值法进行灰度处理   
        //        for (int i = Width; i >= 0; i--)
        //        {
        //            for (int j = Height; j >= 0; j--)
        //            {
        //                color = image2.GetPixel(i, j);
        //                int tmp = Math.Max(color.R, color.G);
        //                int maxcolor = Math.Max(tmp, color.B);
        //                Color colorResult = Color.FromArgb(255, maxcolor, maxcolor, maxcolor);
        //                //设置处理后的灰度信息   
        //                image2.SetPixel(i, j, colorResult);
        //            }
        //        }

        //        //重新绘制灰度化图   
        //        g.DrawImage(image2, new Rectangle(0, 0, Width, Height));
        //        //在第二行绘制图片   
        //        g.ResetTransform();
        //        //g.TranslateTransform(0, image.Height);

        //        //使用加权平均法进行灰度处理	   
        //        for (int i = Width; i >= 0; i--)
        //        {
        //            for (int j = Height; j >= 0; j--)
        //            {
        //                color = image3.GetPixel(i, j);
        //                int R = (int)(0.3f * color.R);
        //                int G = (int)(0.59f * color.G);
        //                int B = (int)(0.11f * color.B);

        //                Color colorResult = Color.FromArgb(255, R, G, B);
        //                //设置处理后的灰度信息   
        //                image3.SetPixel(i, j, colorResult);
        //            }
        //        }
        //        //重新绘制灰度化图   
        //        //g.DrawImage(image3, new Rectangle(0, 0, Width, Height));

        //        //g.TranslateTransform(image.Width, 0);
        //        //灰度的还原演示，还原使用最大值法处理的灰度图像image2   
        //        for (int i = Width; i > 0; i--)
        //        {
        //            for (int j = Height; j > 0; j--)
        //            {
        //                color = image2.GetPixel(i, j);
        //                int R = color.R;
        //                int G = color.G;
        //                int B = color.B;
        //                //分别对RGB三种色彩分量进行伪彩色还原   

        //                //进行红色分量的还原   
        //                if (R < 127)
        //                    R = 0;
        //                if (R >= 192)
        //                    R = 255;
        //                if (R <= 191 && R >= 128)
        //                    R = 4 * R - 510;

        //                /*进行绿色分量的还原,为了还原后的绿色分量再次参加比较，   
        //                这里设置一个变量YES表示G是否已经参加了比较*/

        //                bool yes;
        //                yes = false;
        //                if (G <= 191 && G >= 128 && (!yes))
        //                {
        //                    G = 255;
        //                    yes = true;
        //                }
        //                if (G >= 192 && (!yes))
        //                {
        //                    G = 1022 - 4 * G;
        //                    yes = true;
        //                }
        //                if (G <= 63 && (!yes))
        //                {
        //                    G = 254 - 4 * G;
        //                    yes = true;
        //                }
        //                if (G <= 127 && G >= 67 && (!yes))
        //                    G = 4 * G - 257;

        //                //进行蓝色分量的还原   
        //                if (B <= 63)
        //                    B = 255;
        //                if (B >= 128)
        //                    B = 0;
        //                if (B >= 67 && B <= 127)
        //                    B = 510 - 4 * B;

        //                //还原后的伪彩色   
        //                Color colorResult = Color.FromArgb(255, R, G, B);
        //                //将还原后的RGB信息重新写入位图   
        //                image2.SetPixel(i, j, colorResult);

        //            }
        //        }
        //        //重新绘制还原后的伪彩色位图   
        //        //重新绘制灰度化图   
        //        //g.DrawImage(image2, new Rectangle(0, 0, Width, Height));
        //    }

        //    return Ret;
        //}

        #endregion

        //去色
        static public Bitmap DelColor(Image souImage)
        {
            Bitmap Ret = new Bitmap(souImage);

            //使用最大值法进行灰度处理   
            for (int i = Ret.Width - 1; i >= 0; i--)
            {
                for (int j = Ret.Height - 1; j >= 0; j--)
                {
                    Color color = Ret.GetPixel(i, j);
                    int tmp = Math.Max(color.R, color.G);
                    int maxcolor = Math.Max(tmp, color.B);
                    Color colorResult = Color.FromArgb(color.A, maxcolor, maxcolor, maxcolor);
                    //设置处理后的灰度信息   
                    Ret.SetPixel(i, j, colorResult);
                }
            }

            return Ret;
        }

        //变色
        static public Bitmap ChangeColor(Image souImage, Color NewColor)
        {
            Bitmap Ret = new Bitmap(souImage);

            for (int i = Ret.Width - 1; i >= 0; i--)
            {
                for (int j = Ret.Height - 1; j >= 0; j--)
                {
                    Color color = Ret.GetPixel(i, j);
                    if (color.A > 0)
                    {
                        //设置处理后的灰度信息   
                        Ret.SetPixel(i, j, NewColor);
                    }
                }
            }

            return Ret;
        }
    }
}
