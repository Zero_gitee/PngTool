﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PngTool
{
    /// <summary>
    /// frmToolbar.xaml 的交互逻辑
    /// </summary>
    public partial class frmToolbar : Window
    {
        static private frmToolbar _frm = null;
        static public void ShowToolbar()
        {
            MainWindow main = Application.Current.MainWindow as MainWindow;
            if (_frm == null)
            {
                _frm = new frmToolbar(main);
            }

            if (main.IsMouseOver)
            {
                Point p = Mouse.GetPosition(main);
                _frm.Left = main.Left + p.X + 5;
                _frm.Top = main.Top + p.Y - 25;
            }
            else
            {
                _frm.Left = main.Left + 25;
                _frm.Top = main.Top + 25;
            }

            if (_frm.Visibility != Visibility.Visible)
            {
                _frm.Show();
            }
        }

        static public void HideToolbar()
        {
            if (_frm != null)
            {
                if ((!_frm.Owner.IsMouseOver) && (!_frm.IsMouseOver))
                {
                    _frm.Hide();
                }
            }
        }

        static public bool IsShowToolbar
        {
            get
            {
                return (_frm != null) && (_frm.Visibility == Visibility.Visible);
            }
            set
            {
                if (value)
                {
                    ShowToolbar();
                }
                else
                {
                    HideToolbar();
                }
            }
        }

        private frmToolbar(MainWindow main)
        {
            InitializeComponent();

            Owner = main;

            KeyDown += (sender, e) =>
                {
                    main.ShortcutKey(e);
                };

            MouseLeave += (sender, e) =>
                {
                    this.Hide();
                };

            #region 工具栏按钮事件

            btnZoomOut.MouseLeftButtonDown += (sender, e) =>
            {
                main.ZoomOut();
            };

            btnZoomIn.MouseLeftButtonDown += (sender, e) =>
            {
                main.ZoomIn();
            };

            btnClockwiseRotation.MouseLeftButtonDown += (sender, e) =>
            {
                main.ClockwiseRotation();
            };

            btnCounterClockwiseRotation.MouseLeftButtonDown += (sender, e) =>
            {
                main.CounterClockwiseRotation();
            };

            btnReversalUpDown.MouseLeftButtonDown += (sender, e) =>
            {
                main.ReversalUpDown();
            };

            btnReversalLeftRight.MouseLeftButtonDown += (sender, e) =>
            {
                main.ReversalLeftRight();
            };

            btnReSet.MouseLeftButtonDown += (sender, e) =>
            {
                main.ReSet();
            };

            #endregion
        }
    }
}
