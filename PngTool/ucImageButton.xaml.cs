﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace PngTool
{
    /// <summary>
    /// unImageButton.xaml 的交互逻辑
    /// </summary>
    public partial class ucImageButton : UserControl
    {
        public Color Color
        {
            set
            {
                effect.Color = value;
            }
        }

        public ImageSource Source
        {
            set
            {
                image.Source = value;
            }
        }

        public int BlurRadius
        {
            set
            {
                effect.BlurRadius = value;
            }
        }

        public ucImageButton()
        {
            InitializeComponent();

            MouseEnter += new MouseEventHandler(unImageButton_MouseEnter);
            MouseLeave += new MouseEventHandler(unImageButton_MouseLeave);
            MouseLeftButtonDown += new MouseButtonEventHandler(unImageButton_MouseLeftButtonDown);
            MouseUp += new MouseButtonEventHandler(unImageButton_MouseUp);
        }

        void unImageButton_MouseEnter(object sender, MouseEventArgs e)
        {
            //effect.Opacity = 0.8;
            (this.Resources["styUnLight"] as Storyboard).Stop();
            (this.Resources["styLight"] as Storyboard).Begin();
        }

        void unImageButton_MouseLeave(object sender, MouseEventArgs e)
        {
            //effect.Opacity = 0;
            (this.Resources["styLight"] as Storyboard).Stop();
            (this.Resources["styUnLight"] as Storyboard).Begin();

            renderTransform.X = 0;
            renderTransform.Y = 0;
            //(this.Resources["styTranslate"] as Storyboard).Stop();
            //(this.Resources["styUnTranslate"] as Storyboard).Begin();
        }

        void unImageButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            renderTransform.X = 1;
            renderTransform.Y = 1;
            //(this.Resources["styTranslate"] as Storyboard).Begin();
            //(this.Resources["styUnTranslate"] as Storyboard).Stop();
        }

        void unImageButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            renderTransform.X = 0;
            renderTransform.Y = 0;
            //(this.Resources["styTranslate"] as Storyboard).Stop();
            //(this.Resources["styUnTranslate"] as Storyboard).Begin();
        }
    }
}
